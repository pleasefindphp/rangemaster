-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2015 at 08:22 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rangemaster`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp7664786784_commentmeta`
--

CREATE TABLE IF NOT EXISTS `wp7664786784_commentmeta` (
`meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `wp7664786784_commentmeta`
--

INSERT INTO `wp7664786784_commentmeta` (`meta_id`, `comment_id`, `meta_key`, `meta_value`) VALUES
(1, 1, '_wp_trash_meta_status', '1'),
(2, 1, '_wp_trash_meta_time', '1424362771');

-- --------------------------------------------------------

--
-- Table structure for table `wp7664786784_comments`
--

CREATE TABLE IF NOT EXISTS `wp7664786784_comments` (
`comment_ID` bigint(20) unsigned NOT NULL,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `wp7664786784_comments`
--

INSERT INTO `wp7664786784_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Mr WordPress', '', 'https://wordpress.org/', '', '2015-02-18 13:09:37', '2015-02-18 13:09:37', 'Hi, this is a comment.\nTo delete a comment, just log in and view the post&#039;s comments. There you will have the option to edit or delete them.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp7664786784_links`
--

CREATE TABLE IF NOT EXISTS `wp7664786784_links` (
`link_id` bigint(20) unsigned NOT NULL,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wp7664786784_options`
--

CREATE TABLE IF NOT EXISTS `wp7664786784_options` (
`option_id` bigint(20) unsigned NOT NULL,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=281 ;

--
-- Dumping data for table `wp7664786784_options`
--

INSERT INTO `wp7664786784_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/rangemaster', 'yes'),
(2, 'home', 'http://localhost/rangemaster', 'yes'),
(3, 'blogname', 'rangemaster', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'hitendra.singh2007@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'gzipcompression', '0', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:36:"contact-form-7/wp-contact-form-7.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'advanced_edit', '0', 'yes'),
(37, 'comment_max_links', '2', 'yes'),
(38, 'gmt_offset', '0', 'yes'),
(39, 'default_email_category', '1', 'yes'),
(40, 'recently_edited', '', 'no'),
(41, 'template', 'rangemaster', 'yes'),
(42, 'stylesheet', 'rangemaster', 'yes'),
(43, 'comment_whitelist', '1', 'yes'),
(44, 'blacklist_keys', '', 'no'),
(45, 'comment_registration', '', 'yes'),
(46, 'html_type', 'text/html', 'yes'),
(47, 'use_trackback', '0', 'yes'),
(48, 'default_role', 'subscriber', 'yes'),
(49, 'db_version', '30133', 'yes'),
(50, 'uploads_use_yearmonth_folders', '1', 'yes'),
(51, 'upload_path', '', 'yes'),
(52, 'blog_public', '0', 'yes'),
(53, 'default_link_category', '2', 'yes'),
(54, 'show_on_front', 'page', 'yes'),
(55, 'tag_base', '', 'yes'),
(56, 'show_avatars', '1', 'yes'),
(57, 'avatar_rating', 'G', 'yes'),
(58, 'upload_url_path', '', 'yes'),
(59, 'thumbnail_size_w', '150', 'yes'),
(60, 'thumbnail_size_h', '150', 'yes'),
(61, 'thumbnail_crop', '1', 'yes'),
(62, 'medium_size_w', '300', 'yes'),
(63, 'medium_size_h', '300', 'yes'),
(64, 'avatar_default', 'mystery', 'yes'),
(65, 'large_size_w', '1024', 'yes'),
(66, 'large_size_h', '1024', 'yes'),
(67, 'image_default_link_type', 'file', 'yes'),
(68, 'image_default_size', '', 'yes'),
(69, 'image_default_align', '', 'yes'),
(70, 'close_comments_for_old_posts', '', 'yes'),
(71, 'close_comments_days_old', '14', 'yes'),
(72, 'thread_comments', '1', 'yes'),
(73, 'thread_comments_depth', '5', 'yes'),
(74, 'page_comments', '', 'yes'),
(75, 'comments_per_page', '50', 'yes'),
(76, 'default_comments_page', 'newest', 'yes'),
(77, 'comment_order', 'asc', 'yes'),
(78, 'sticky_posts', 'a:0:{}', 'yes'),
(79, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_text', 'a:0:{}', 'yes'),
(81, 'widget_rss', 'a:0:{}', 'yes'),
(82, 'uninstall_plugins', 'a:0:{}', 'no'),
(83, 'timezone_string', '', 'yes'),
(84, 'page_for_posts', '0', 'yes'),
(85, 'page_on_front', '19', 'yes'),
(86, 'default_post_format', '0', 'yes'),
(87, 'link_manager_enabled', '0', 'yes'),
(88, 'initial_db_version', '30133', 'yes'),
(89, 'wp7664786784_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:62:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:9:"add_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(90, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(91, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(92, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(93, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(96, 'cron', 'a:6:{i:1425474591;a:2:{s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1425479396;a:1:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1425486418;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1425496225;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1425499080;a:1:{s:20:"wp_maybe_auto_update";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}s:7:"version";i:2;}', 'yes'),
(101, '_transient_random_seed', '8f2ec045300b093bc4d54784d6ef90d0', 'yes'),
(104, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1425455958;s:7:"checked";a:4:{s:11:"rangemaster";s:0:"";s:13:"twentyfifteen";s:3:"1.0";s:14:"twentyfourteen";s:3:"1.3";s:14:"twentythirteen";s:3:"1.4";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'yes'),
(105, '_site_transient_timeout_browser_eeaadcdae20e79a8498ae96f41e4f2fc', '1424869807', 'yes'),
(106, '_site_transient_browser_eeaadcdae20e79a8498ae96f41e4f2fc', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"39.0.2171.65";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(107, 'can_compress_scripts', '1', 'yes'),
(129, 'theme_mods_twentyfifteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1424276839;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(130, 'current_theme', 'Range Master', 'yes'),
(131, 'theme_mods_rangemaster', 'a:1:{i:0;b:0;}', 'yes'),
(132, 'theme_switched', '', 'yes'),
(149, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.1.1.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.1.1.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.1.1-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.1.1-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.1.1";s:7:"version";s:5:"4.1.1";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1425446356;s:15:"version_checked";s:5:"4.1.1";s:12:"translations";a:0:{}}', 'yes'),
(150, 'auto_core_update_notified', 'a:4:{s:4:"type";s:7:"success";s:5:"email";s:28:"hitendra.singh2007@gmail.com";s:7:"version";s:5:"4.1.1";s:9:"timestamp";i:1424321479;}', 'yes'),
(171, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1424372634', 'yes'),
(172, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'a:40:{s:6:"widget";a:3:{s:4:"name";s:6:"widget";s:4:"slug";s:6:"widget";s:5:"count";s:4:"4916";}s:4:"post";a:3:{s:4:"name";s:4:"Post";s:4:"slug";s:4:"post";s:5:"count";s:4:"3078";}s:6:"plugin";a:3:{s:4:"name";s:6:"plugin";s:4:"slug";s:6:"plugin";s:5:"count";s:4:"3022";}s:5:"admin";a:3:{s:4:"name";s:5:"admin";s:4:"slug";s:5:"admin";s:5:"count";s:4:"2529";}s:5:"posts";a:3:{s:4:"name";s:5:"posts";s:4:"slug";s:5:"posts";s:5:"count";s:4:"2346";}s:7:"sidebar";a:3:{s:4:"name";s:7:"sidebar";s:4:"slug";s:7:"sidebar";s:5:"count";s:4:"1892";}s:6:"google";a:3:{s:4:"name";s:6:"google";s:4:"slug";s:6:"google";s:5:"count";s:4:"1729";}s:7:"twitter";a:3:{s:4:"name";s:7:"twitter";s:4:"slug";s:7:"twitter";s:5:"count";s:4:"1680";}s:9:"shortcode";a:3:{s:4:"name";s:9:"shortcode";s:4:"slug";s:9:"shortcode";s:5:"count";s:4:"1678";}s:6:"images";a:3:{s:4:"name";s:6:"images";s:4:"slug";s:6:"images";s:5:"count";s:4:"1676";}s:8:"comments";a:3:{s:4:"name";s:8:"comments";s:4:"slug";s:8:"comments";s:5:"count";s:4:"1612";}s:4:"page";a:3:{s:4:"name";s:4:"page";s:4:"slug";s:4:"page";s:5:"count";s:4:"1609";}s:5:"image";a:3:{s:4:"name";s:5:"image";s:4:"slug";s:5:"image";s:5:"count";s:4:"1505";}s:8:"facebook";a:3:{s:4:"name";s:8:"Facebook";s:4:"slug";s:8:"facebook";s:5:"count";s:4:"1322";}s:3:"seo";a:3:{s:4:"name";s:3:"seo";s:4:"slug";s:3:"seo";s:5:"count";s:4:"1276";}s:9:"wordpress";a:3:{s:4:"name";s:9:"wordpress";s:4:"slug";s:9:"wordpress";s:5:"count";s:4:"1175";}s:5:"links";a:3:{s:4:"name";s:5:"links";s:4:"slug";s:5:"links";s:5:"count";s:4:"1171";}s:7:"gallery";a:3:{s:4:"name";s:7:"gallery";s:4:"slug";s:7:"gallery";s:5:"count";s:4:"1083";}s:6:"social";a:3:{s:4:"name";s:6:"social";s:4:"slug";s:6:"social";s:5:"count";s:4:"1079";}s:5:"email";a:3:{s:4:"name";s:5:"email";s:4:"slug";s:5:"email";s:5:"count";s:3:"918";}s:7:"widgets";a:3:{s:4:"name";s:7:"widgets";s:4:"slug";s:7:"widgets";s:5:"count";s:3:"905";}s:5:"pages";a:3:{s:4:"name";s:5:"pages";s:4:"slug";s:5:"pages";s:5:"count";s:3:"874";}s:6:"jquery";a:3:{s:4:"name";s:6:"jquery";s:4:"slug";s:6:"jquery";s:5:"count";s:3:"843";}s:3:"rss";a:3:{s:4:"name";s:3:"rss";s:4:"slug";s:3:"rss";s:5:"count";s:3:"837";}s:5:"media";a:3:{s:4:"name";s:5:"media";s:4:"slug";s:5:"media";s:5:"count";s:3:"794";}s:5:"video";a:3:{s:4:"name";s:5:"video";s:4:"slug";s:5:"video";s:5:"count";s:3:"758";}s:4:"ajax";a:3:{s:4:"name";s:4:"AJAX";s:4:"slug";s:4:"ajax";s:5:"count";s:3:"748";}s:7:"content";a:3:{s:4:"name";s:7:"content";s:4:"slug";s:7:"content";s:5:"count";s:3:"709";}s:11:"woocommerce";a:3:{s:4:"name";s:11:"woocommerce";s:4:"slug";s:11:"woocommerce";s:5:"count";s:3:"700";}s:10:"javascript";a:3:{s:4:"name";s:10:"javascript";s:4:"slug";s:10:"javascript";s:5:"count";s:3:"692";}s:5:"login";a:3:{s:4:"name";s:5:"login";s:4:"slug";s:5:"login";s:5:"count";s:3:"682";}s:5:"photo";a:3:{s:4:"name";s:5:"photo";s:4:"slug";s:5:"photo";s:5:"count";s:3:"657";}s:10:"buddypress";a:3:{s:4:"name";s:10:"buddypress";s:4:"slug";s:10:"buddypress";s:5:"count";s:3:"649";}s:4:"feed";a:3:{s:4:"name";s:4:"feed";s:4:"slug";s:4:"feed";s:5:"count";s:3:"642";}s:4:"link";a:3:{s:4:"name";s:4:"link";s:4:"slug";s:4:"link";s:5:"count";s:3:"642";}s:9:"ecommerce";a:3:{s:4:"name";s:9:"ecommerce";s:4:"slug";s:9:"ecommerce";s:5:"count";s:3:"623";}s:6:"photos";a:3:{s:4:"name";s:6:"photos";s:4:"slug";s:6:"photos";s:5:"count";s:3:"620";}s:7:"youtube";a:3:{s:4:"name";s:7:"youtube";s:4:"slug";s:7:"youtube";s:5:"count";s:3:"605";}s:5:"share";a:3:{s:4:"name";s:5:"Share";s:4:"slug";s:5:"share";s:5:"count";s:3:"600";}s:4:"spam";a:3:{s:4:"name";s:4:"spam";s:4:"slug";s:4:"spam";s:5:"count";s:3:"593";}}', 'yes'),
(173, '_site_transient_update_plugins', 'O:8:"stdClass":4:{s:12:"last_checked";i:1425455934;s:8:"response";a:0:{}s:12:"translations";a:0:{}s:9:"no_update";a:3:{s:19:"akismet/akismet.php";O:8:"stdClass":6:{s:2:"id";s:2:"15";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"3.0.4";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/akismet.3.0.4.zip";}s:36:"contact-form-7/wp-contact-form-7.php";O:8:"stdClass":6:{s:2:"id";s:3:"790";s:4:"slug";s:14:"contact-form-7";s:6:"plugin";s:36:"contact-form-7/wp-contact-form-7.php";s:11:"new_version";s:3:"4.1";s:3:"url";s:45:"https://wordpress.org/plugins/contact-form-7/";s:7:"package";s:61:"https://downloads.wordpress.org/plugin/contact-form-7.4.1.zip";}s:9:"hello.php";O:8:"stdClass":6:{s:2:"id";s:4:"3564";s:4:"slug";s:11:"hello-dolly";s:6:"plugin";s:9:"hello.php";s:11:"new_version";s:3:"1.6";s:3:"url";s:42:"https://wordpress.org/plugins/hello-dolly/";s:7:"package";s:58:"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip";}}}', 'yes'),
(174, 'wpcf7', 'a:1:{s:7:"version";s:3:"4.1";}', 'yes'),
(175, 'recently_activated', 'a:0:{}', 'yes'),
(178, 'WPLANG', '', 'yes'),
(207, '_site_transient_timeout_browser_8c6bd500ecb4268b8bc980c6a9bea261', '1425798693', 'yes'),
(208, '_site_transient_browser_8c6bd500ecb4268b8bc980c6a9bea261', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:13:"40.0.2214.115";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(259, 'category_children', 'a:0:{}', 'yes'),
(260, '_site_transient_timeout_available_translations', '1425298289', 'yes'),
(261, '_site_transient_available_translations', 'a:51:{s:2:"ar";a:8:{s:8:"language";s:2:"ar";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-17 19:01:24";s:12:"english_name";s:6:"Arabic";s:11:"native_name";s:14:"العربية";s:7:"package";s:60:"http://downloads.wordpress.org/translation/core/4.1.1/ar.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:2;s:3:"ara";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:2:"az";a:8:{s:8:"language";s:2:"az";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-27 15:23:28";s:12:"english_name";s:11:"Azerbaijani";s:11:"native_name";s:16:"Azərbaycan dili";s:7:"package";s:60:"http://downloads.wordpress.org/translation/core/4.1.1/az.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:2;s:3:"aze";}s:7:"strings";a:1:{s:8:"continue";s:5:"Davam";}}s:5:"bg_BG";a:8:{s:8:"language";s:5:"bg_BG";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-18 11:10:33";s:12:"english_name";s:9:"Bulgarian";s:11:"native_name";s:18:"Български";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/bg_BG.zip";s:3:"iso";a:2:{i:1;s:2:"bg";i:2;s:3:"bul";}s:7:"strings";a:1:{s:8:"continue";s:22:"Продължение";}}s:5:"bs_BA";a:8:{s:8:"language";s:5:"bs_BA";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-08 17:39:56";s:12:"english_name";s:7:"Bosnian";s:11:"native_name";s:8:"Bosanski";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/bs_BA.zip";s:3:"iso";a:2:{i:1;s:2:"bs";i:2;s:3:"bos";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:2:"ca";a:8:{s:8:"language";s:2:"ca";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-26 03:38:28";s:12:"english_name";s:7:"Catalan";s:11:"native_name";s:7:"Català";s:7:"package";s:60:"http://downloads.wordpress.org/translation/core/4.1.1/ca.zip";s:3:"iso";a:2:{i:1;s:2:"ca";i:2;s:3:"cat";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"cy";a:8:{s:8:"language";s:2:"cy";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-09 11:12:57";s:12:"english_name";s:5:"Welsh";s:11:"native_name";s:7:"Cymraeg";s:7:"package";s:60:"http://downloads.wordpress.org/translation/core/4.1.1/cy.zip";s:3:"iso";a:2:{i:1;s:2:"cy";i:2;s:3:"cym";}s:7:"strings";a:1:{s:8:"continue";s:6:"Parhau";}}s:5:"da_DK";a:8:{s:8:"language";s:5:"da_DK";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-18 22:27:33";s:12:"english_name";s:6:"Danish";s:11:"native_name";s:5:"Dansk";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/da_DK.zip";s:3:"iso";a:2:{i:1;s:2:"da";i:2;s:3:"dan";}s:7:"strings";a:1:{s:8:"continue";s:12:"Forts&#230;t";}}s:5:"de_CH";a:8:{s:8:"language";s:5:"de_CH";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-04 12:59:40";s:12:"english_name";s:20:"German (Switzerland)";s:11:"native_name";s:17:"Deutsch (Schweiz)";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/de_CH.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:5:"de_DE";a:8:{s:8:"language";s:5:"de_DE";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-18 11:05:07";s:12:"english_name";s:6:"German";s:11:"native_name";s:7:"Deutsch";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/de_DE.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:2:"el";a:8:{s:8:"language";s:2:"el";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-26 19:02:34";s:12:"english_name";s:5:"Greek";s:11:"native_name";s:16:"Ελληνικά";s:7:"package";s:60:"http://downloads.wordpress.org/translation/core/4.1.1/el.zip";s:3:"iso";a:2:{i:1;s:2:"el";i:2;s:3:"ell";}s:7:"strings";a:1:{s:8:"continue";s:16:"Συνέχεια";}}s:5:"en_GB";a:8:{s:8:"language";s:5:"en_GB";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-17 20:53:36";s:12:"english_name";s:12:"English (UK)";s:11:"native_name";s:12:"English (UK)";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/en_GB.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_CA";a:8:{s:8:"language";s:5:"en_CA";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-28 01:01:02";s:12:"english_name";s:16:"English (Canada)";s:11:"native_name";s:16:"English (Canada)";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/en_CA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_AU";a:8:{s:8:"language";s:5:"en_AU";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-05 09:59:30";s:12:"english_name";s:19:"English (Australia)";s:11:"native_name";s:19:"English (Australia)";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/en_AU.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:2:"eo";a:8:{s:8:"language";s:2:"eo";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-18 13:11:32";s:12:"english_name";s:9:"Esperanto";s:11:"native_name";s:9:"Esperanto";s:7:"package";s:60:"http://downloads.wordpress.org/translation/core/4.1.1/eo.zip";s:3:"iso";a:2:{i:1;s:2:"eo";i:2;s:3:"epo";}s:7:"strings";a:1:{s:8:"continue";s:8:"Daŭrigi";}}s:5:"es_MX";a:8:{s:8:"language";s:5:"es_MX";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-05 15:18:10";s:12:"english_name";s:16:"Spanish (Mexico)";s:11:"native_name";s:19:"Español de México";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/es_MX.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_ES";a:8:{s:8:"language";s:5:"es_ES";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-25 14:34:19";s:12:"english_name";s:15:"Spanish (Spain)";s:11:"native_name";s:8:"Español";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/es_ES.zip";s:3:"iso";a:1:{i:1;s:2:"es";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_PE";a:8:{s:8:"language";s:5:"es_PE";s:7:"version";s:3:"4.1";s:7:"updated";s:19:"2014-12-19 08:14:32";s:12:"english_name";s:14:"Spanish (Peru)";s:11:"native_name";s:17:"Español de Perú";s:7:"package";s:61:"http://downloads.wordpress.org/translation/core/4.1/es_PE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CL";a:8:{s:8:"language";s:5:"es_CL";s:7:"version";s:3:"4.0";s:7:"updated";s:19:"2014-09-04 19:47:01";s:12:"english_name";s:15:"Spanish (Chile)";s:11:"native_name";s:17:"Español de Chile";s:7:"package";s:61:"http://downloads.wordpress.org/translation/core/4.0/es_CL.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"eu";a:8:{s:8:"language";s:2:"eu";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-09 12:20:08";s:12:"english_name";s:6:"Basque";s:11:"native_name";s:7:"Euskara";s:7:"package";s:60:"http://downloads.wordpress.org/translation/core/4.1.1/eu.zip";s:3:"iso";a:2:{i:1;s:2:"eu";i:2;s:3:"eus";}s:7:"strings";a:1:{s:8:"continue";s:8:"Jarraitu";}}s:5:"fa_IR";a:8:{s:8:"language";s:5:"fa_IR";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-23 14:29:09";s:12:"english_name";s:7:"Persian";s:11:"native_name";s:10:"فارسی";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/fa_IR.zip";s:3:"iso";a:2:{i:1;s:2:"fa";i:2;s:3:"fas";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:2:"fi";a:8:{s:8:"language";s:2:"fi";s:7:"version";s:3:"4.1";s:7:"updated";s:19:"2014-12-17 07:01:16";s:12:"english_name";s:7:"Finnish";s:11:"native_name";s:5:"Suomi";s:7:"package";s:58:"http://downloads.wordpress.org/translation/core/4.1/fi.zip";s:3:"iso";a:2:{i:1;s:2:"fi";i:2;s:3:"fin";}s:7:"strings";a:1:{s:8:"continue";s:5:"Jatka";}}s:5:"fr_FR";a:8:{s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-17 19:01:48";s:12:"english_name";s:15:"French (France)";s:11:"native_name";s:9:"Français";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/fr_FR.zip";s:3:"iso";a:1:{i:1;s:2:"fr";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:2:"gd";a:8:{s:8:"language";s:2:"gd";s:7:"version";s:3:"4.0";s:7:"updated";s:19:"2014-09-05 17:37:43";s:12:"english_name";s:15:"Scottish Gaelic";s:11:"native_name";s:9:"Gàidhlig";s:7:"package";s:58:"http://downloads.wordpress.org/translation/core/4.0/gd.zip";s:3:"iso";a:3:{i:1;s:2:"gd";i:2;s:3:"gla";i:3;s:3:"gla";}s:7:"strings";a:1:{s:8:"continue";s:15:"Lean air adhart";}}s:5:"gl_ES";a:8:{s:8:"language";s:5:"gl_ES";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-17 18:37:43";s:12:"english_name";s:8:"Galician";s:11:"native_name";s:6:"Galego";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/gl_ES.zip";s:3:"iso";a:2:{i:1;s:2:"gl";i:2;s:3:"glg";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:3:"haz";a:8:{s:8:"language";s:3:"haz";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-12 01:05:09";s:12:"english_name";s:8:"Hazaragi";s:11:"native_name";s:15:"هزاره گی";s:7:"package";s:61:"http://downloads.wordpress.org/translation/core/4.1.1/haz.zip";s:3:"iso";a:2:{i:1;s:3:"haz";i:2;s:3:"haz";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:5:"he_IL";a:8:{s:8:"language";s:5:"he_IL";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-03-01 00:26:14";s:12:"english_name";s:6:"Hebrew";s:11:"native_name";s:16:"עִבְרִית";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/he_IL.zip";s:3:"iso";a:1:{i:1;s:2:"he";}s:7:"strings";a:1:{s:8:"continue";s:12:"להמשיך";}}s:2:"hr";a:8:{s:8:"language";s:2:"hr";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-20 16:50:00";s:12:"english_name";s:8:"Croatian";s:11:"native_name";s:8:"Hrvatski";s:7:"package";s:60:"http://downloads.wordpress.org/translation/core/4.1.1/hr.zip";s:3:"iso";a:2:{i:1;s:2:"hr";i:2;s:3:"hrv";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:5:"hu_HU";a:8:{s:8:"language";s:5:"hu_HU";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-15 20:01:36";s:12:"english_name";s:9:"Hungarian";s:11:"native_name";s:6:"Magyar";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/hu_HU.zip";s:3:"iso";a:2:{i:1;s:2:"hu";i:2;s:3:"hun";}s:7:"strings";a:1:{s:8:"continue";s:7:"Tovább";}}s:5:"id_ID";a:8:{s:8:"language";s:5:"id_ID";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-31 07:30:24";s:12:"english_name";s:10:"Indonesian";s:11:"native_name";s:16:"Bahasa Indonesia";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/id_ID.zip";s:3:"iso";a:2:{i:1;s:2:"id";i:2;s:3:"ind";}s:7:"strings";a:1:{s:8:"continue";s:9:"Lanjutkan";}}s:5:"is_IS";a:8:{s:8:"language";s:5:"is_IS";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-26 01:33:47";s:12:"english_name";s:9:"Icelandic";s:11:"native_name";s:9:"Íslenska";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/is_IS.zip";s:3:"iso";a:2:{i:1;s:2:"is";i:2;s:3:"isl";}s:7:"strings";a:1:{s:8:"continue";s:6:"Áfram";}}s:5:"it_IT";a:8:{s:8:"language";s:5:"it_IT";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-21 07:43:12";s:12:"english_name";s:7:"Italian";s:11:"native_name";s:8:"Italiano";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/it_IT.zip";s:3:"iso";a:2:{i:1;s:2:"it";i:2;s:3:"ita";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"ja";a:8:{s:8:"language";s:2:"ja";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-29 10:53:40";s:12:"english_name";s:8:"Japanese";s:11:"native_name";s:9:"日本語";s:7:"package";s:60:"http://downloads.wordpress.org/translation/core/4.1.1/ja.zip";s:3:"iso";a:1:{i:1;s:2:"ja";}s:7:"strings";a:1:{s:8:"continue";s:9:"続ける";}}s:5:"ko_KR";a:8:{s:8:"language";s:5:"ko_KR";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-21 03:05:42";s:12:"english_name";s:6:"Korean";s:11:"native_name";s:9:"한국어";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/ko_KR.zip";s:3:"iso";a:2:{i:1;s:2:"ko";i:2;s:3:"kor";}s:7:"strings";a:1:{s:8:"continue";s:6:"계속";}}s:5:"lt_LT";a:8:{s:8:"language";s:5:"lt_LT";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-08 00:36:50";s:12:"english_name";s:10:"Lithuanian";s:11:"native_name";s:15:"Lietuvių kalba";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/lt_LT.zip";s:3:"iso";a:2:{i:1;s:2:"lt";i:2;s:3:"lit";}s:7:"strings";a:1:{s:8:"continue";s:6:"Tęsti";}}s:5:"my_MM";a:8:{s:8:"language";s:5:"my_MM";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-23 10:05:46";s:12:"english_name";s:7:"Burmese";s:11:"native_name";s:15:"ဗမာစာ";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/my_MM.zip";s:3:"iso";a:2:{i:1;s:2:"my";i:2;s:3:"mya";}s:7:"strings";a:1:{s:8:"continue";s:54:"ဆက်လက်လုပ်ေဆာင်ပါ။";}}s:5:"nb_NO";a:8:{s:8:"language";s:5:"nb_NO";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-18 11:09:37";s:12:"english_name";s:19:"Norwegian (Bokmål)";s:11:"native_name";s:13:"Norsk bokmål";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/nb_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nb";i:2;s:3:"nob";}s:7:"strings";a:1:{s:8:"continue";s:8:"Fortsett";}}s:5:"nl_NL";a:8:{s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-18 13:44:24";s:12:"english_name";s:5:"Dutch";s:11:"native_name";s:10:"Nederlands";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/nl_NL.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"pl_PL";a:8:{s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-16 15:47:22";s:12:"english_name";s:6:"Polish";s:11:"native_name";s:6:"Polski";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/pl_PL.zip";s:3:"iso";a:2:{i:1;s:2:"pl";i:2;s:3:"pol";}s:7:"strings";a:1:{s:8:"continue";s:9:"Kontynuuj";}}s:5:"pt_PT";a:8:{s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-25 20:46:09";s:12:"english_name";s:21:"Portuguese (Portugal)";s:11:"native_name";s:10:"Português";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/pt_PT.zip";s:3:"iso";a:1:{i:1;s:2:"pt";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"pt_BR";a:8:{s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-19 19:37:03";s:12:"english_name";s:19:"Portuguese (Brazil)";s:11:"native_name";s:20:"Português do Brasil";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/pt_BR.zip";s:3:"iso";a:2:{i:1;s:2:"pt";i:2;s:3:"por";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"ro_RO";a:8:{s:8:"language";s:5:"ro_RO";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-23 20:32:43";s:12:"english_name";s:8:"Romanian";s:11:"native_name";s:8:"Română";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/ro_RO.zip";s:3:"iso";a:2:{i:1;s:2:"ro";i:2;s:3:"ron";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuă";}}s:5:"ru_RU";a:8:{s:8:"language";s:5:"ru_RU";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-17 18:16:58";s:12:"english_name";s:7:"Russian";s:11:"native_name";s:14:"Русский";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/ru_RU.zip";s:3:"iso";a:2:{i:1;s:2:"ru";i:2;s:3:"rus";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продолжить";}}s:5:"sk_SK";a:8:{s:8:"language";s:5:"sk_SK";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-27 12:38:24";s:12:"english_name";s:6:"Slovak";s:11:"native_name";s:11:"Slovenčina";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/sk_SK.zip";s:3:"iso";a:2:{i:1;s:2:"sk";i:2;s:3:"slk";}s:7:"strings";a:1:{s:8:"continue";s:12:"Pokračovať";}}s:5:"sl_SI";a:8:{s:8:"language";s:5:"sl_SI";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-13 22:38:48";s:12:"english_name";s:9:"Slovenian";s:11:"native_name";s:13:"Slovenščina";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/sl_SI.zip";s:3:"iso";a:2:{i:1;s:2:"sl";i:2;s:3:"slv";}s:7:"strings";a:1:{s:8:"continue";s:10:"Nadaljujte";}}s:5:"sr_RS";a:8:{s:8:"language";s:5:"sr_RS";s:7:"version";s:3:"4.1";s:7:"updated";s:19:"2014-12-18 19:08:01";s:12:"english_name";s:7:"Serbian";s:11:"native_name";s:23:"Српски језик";s:7:"package";s:61:"http://downloads.wordpress.org/translation/core/4.1/sr_RS.zip";s:3:"iso";a:2:{i:1;s:2:"sr";i:2;s:3:"srp";}s:7:"strings";a:1:{s:8:"continue";s:14:"Настави";}}s:5:"sv_SE";a:8:{s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-29 09:41:07";s:12:"english_name";s:7:"Swedish";s:11:"native_name";s:7:"Svenska";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/sv_SE.zip";s:3:"iso";a:2:{i:1;s:2:"sv";i:2;s:3:"swe";}s:7:"strings";a:1:{s:8:"continue";s:9:"Fortsätt";}}s:2:"th";a:8:{s:8:"language";s:2:"th";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-26 04:10:43";s:12:"english_name";s:4:"Thai";s:11:"native_name";s:9:"ไทย";s:7:"package";s:60:"http://downloads.wordpress.org/translation/core/4.1.1/th.zip";s:3:"iso";a:2:{i:1;s:2:"th";i:2;s:3:"tha";}s:7:"strings";a:1:{s:8:"continue";s:15:"ต่อไป";}}s:5:"tr_TR";a:8:{s:8:"language";s:5:"tr_TR";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-01-19 08:42:08";s:12:"english_name";s:7:"Turkish";s:11:"native_name";s:8:"Türkçe";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/tr_TR.zip";s:3:"iso";a:2:{i:1;s:2:"tr";i:2;s:3:"tur";}s:7:"strings";a:1:{s:8:"continue";s:5:"Devam";}}s:5:"ug_CN";a:8:{s:8:"language";s:5:"ug_CN";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-19 05:33:04";s:12:"english_name";s:6:"Uighur";s:11:"native_name";s:9:"Uyƣurqə";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/ug_CN.zip";s:3:"iso";a:2:{i:1;s:2:"ug";i:2;s:3:"uig";}s:7:"strings";a:1:{s:8:"continue";s:26:"داۋاملاشتۇرۇش";}}s:5:"zh_TW";a:8:{s:8:"language";s:5:"zh_TW";s:7:"version";s:5:"4.1.1";s:7:"updated";s:19:"2015-02-19 08:39:08";s:12:"english_name";s:16:"Chinese (Taiwan)";s:11:"native_name";s:12:"繁體中文";s:7:"package";s:63:"http://downloads.wordpress.org/translation/core/4.1.1/zh_TW.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}s:5:"zh_CN";a:8:{s:8:"language";s:5:"zh_CN";s:7:"version";s:3:"4.1";s:7:"updated";s:19:"2014-12-26 02:21:02";s:12:"english_name";s:15:"Chinese (China)";s:11:"native_name";s:12:"简体中文";s:7:"package";s:61:"http://downloads.wordpress.org/translation/core/4.1/zh_CN.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"继续";}}}', 'yes'),
(263, 'rewrite_rules', 'a:68:{s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:39:"index.php?&page_id=19&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)(/[0-9]+)?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)(/[0-9]+)?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";}', 'yes'),
(268, '_transient_timeout_plugin_slugs', '1425532988', 'no'),
(269, '_transient_plugin_slugs', 'a:3:{i:0;s:19:"akismet/akismet.php";i:1;s:36:"contact-form-7/wp-contact-form-7.php";i:2;s:9:"hello.php";}', 'no'),
(270, '_transient_timeout_dash_4077549d03da2e451c8b5f002294ff51', '1425489788', 'no'),
(271, '_transient_dash_4077549d03da2e451c8b5f002294ff51', '<div class="rss-widget"><p><strong>RSS Error</strong>: WP HTTP Error: Could not resolve host: wordpress.org</p></div><div class="rss-widget"><p><strong>RSS Error</strong>: WP HTTP Error: Could not resolve host: planet.wordpress.org</p></div><div class="rss-widget"><ul></ul></div>', 'no'),
(277, '_transient_doing_cron', '1425455921.9006130695343017578125', 'yes'),
(278, 'auto_updater.lock', '1425455933', 'no'),
(279, '_site_transient_timeout_theme_roots', '1425457757', 'yes'),
(280, '_site_transient_theme_roots', 'a:4:{s:11:"rangemaster";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:14:"twentyfourteen";s:7:"/themes";s:14:"twentythirteen";s:7:"/themes";}', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp7664786784_postmeta`
--

CREATE TABLE IF NOT EXISTS `wp7664786784_postmeta` (
`meta_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `wp7664786784_postmeta`
--

INSERT INTO `wp7664786784_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1425211417:1'),
(4, 6, '_form', '<p>Your Name (required)<br />\n    [text* your-name] </p>\n\n<p>Your Email (required)<br />\n    [email* your-email] </p>\n\n<p>Your Phone <br />\n    [text* your-phone] </p>\n\n<p>Your Message<br />\n    [textarea your-message] </p>\n\n<p>[submit "Send"]</p>'),
(5, 6, '_mail', 'a:8:{s:7:"subject";s:14:"[your-subject]";s:6:"sender";s:42:"[your-name] <hitendra.singh2007@gmail.com>";s:4:"body";s:177:"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on rangemaster (http://localhost/rangemaster)";s:9:"recipient";s:28:"hitendra.singh2007@gmail.com";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(6, 6, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:14:"[your-subject]";s:6:"sender";s:42:"rangemaster <hitendra.singh2007@gmail.com>";s:4:"body";s:119:"Message Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on rangemaster (http://localhost/rangemaster)";s:9:"recipient";s:12:"[your-email]";s:18:"additional_headers";s:38:"Reply-To: hitendra.singh2007@gmail.com";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(7, 6, '_messages', 'a:23:{s:12:"mail_sent_ok";s:43:"Your message was sent successfully. Thanks.";s:12:"mail_sent_ng";s:93:"Failed to send your message. Please try later or contact the administrator by another method.";s:16:"validation_error";s:74:"Validation errors occurred. Please confirm the fields and submit it again.";s:4:"spam";s:93:"Failed to send your message. Please try later or contact the administrator by another method.";s:12:"accept_terms";s:35:"Please accept the terms to proceed.";s:16:"invalid_required";s:34:"Please fill in the required field.";s:16:"invalid_too_long";s:23:"This input is too long.";s:17:"invalid_too_short";s:24:"This input is too short.";s:17:"captcha_not_match";s:31:"Your entered code is incorrect.";s:14:"invalid_number";s:28:"Number format seems invalid.";s:16:"number_too_small";s:25:"This number is too small.";s:16:"number_too_large";s:25:"This number is too large.";s:13:"invalid_email";s:28:"Email address seems invalid.";s:11:"invalid_url";s:18:"URL seems invalid.";s:11:"invalid_tel";s:31:"Telephone number seems invalid.";s:23:"quiz_answer_not_correct";s:27:"Your answer is not correct.";s:12:"invalid_date";s:26:"Date format seems invalid.";s:14:"date_too_early";s:23:"This date is too early.";s:13:"date_too_late";s:22:"This date is too late.";s:13:"upload_failed";s:22:"Failed to upload file.";s:24:"upload_file_type_invalid";s:30:"This file type is not allowed.";s:21:"upload_file_too_large";s:23:"This file is too large.";s:23:"upload_failed_php_error";s:38:"Failed to upload file. Error occurred.";}'),
(8, 6, '_additional_settings', ''),
(9, 6, '_locale', 'en_US'),
(10, 7, '_edit_last', '1'),
(11, 7, '_edit_lock', '1425207582:1'),
(12, 9, '_wp_attached_file', '2015/02/map.jpg'),
(13, 9, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1231;s:6:"height";i:401;s:4:"file";s:15:"2015/02/map.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"map-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"map-300x98.jpg";s:5:"width";i:300;s:6:"height";i:98;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"map-1024x334.jpg";s:5:"width";i:1024;s:6:"height";i:334;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:15:"map-825x401.jpg";s:5:"width";i:825;s:6:"height";i:401;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(14, 2, '_wp_trash_meta_status', 'publish'),
(15, 2, '_wp_trash_meta_time', '1424362794'),
(16, 16, '_edit_last', '1'),
(17, 16, '_edit_lock', '1425457998:1'),
(18, 19, '_edit_last', '1'),
(19, 19, '_edit_lock', '1425457069:1'),
(20, 1, '_wp_trash_meta_status', 'publish'),
(21, 1, '_wp_trash_meta_time', '1424366062'),
(22, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:5:"trash";}'),
(23, 23, '_edit_last', '1'),
(24, 23, '_edit_lock', '1425447676:1'),
(25, 24, '_wp_attached_file', '2015/02/blog-1.jpg'),
(26, 24, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:264;s:6:"height";i:181;s:4:"file";s:18:"2015/02/blog-1.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"blog-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(27, 23, '_thumbnail_id', '24'),
(29, 26, '_edit_last', '1'),
(30, 26, '_edit_lock', '1425447683:1'),
(31, 27, '_wp_attached_file', '2015/02/blog-2.jpg'),
(32, 27, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:264;s:6:"height";i:181;s:4:"file";s:18:"2015/02/blog-2.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"blog-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(33, 26, '_thumbnail_id', '27'),
(35, 32, '_edit_last', '1'),
(36, 32, '_edit_lock', '1425199361:1'),
(37, 36, '_edit_last', '1'),
(38, 36, '_edit_lock', '1425199413:1'),
(39, 41, '_edit_last', '1'),
(40, 41, '_edit_lock', '1425199627:1'),
(41, 48, '_edit_last', '1'),
(42, 48, '_edit_lock', '1425211827:1'),
(43, 48, '_thumbnail_id', '27'),
(44, 52, '_edit_last', '1'),
(45, 52, '_edit_lock', '1425284674:1'),
(46, 52, 'price', '$500'),
(47, 52, '_thumbnail_id', '24'),
(48, 53, '_edit_last', '1'),
(49, 53, '_edit_lock', '1425220711:1'),
(50, 48, '_wp_trash_meta_status', 'publish'),
(51, 48, '_wp_trash_meta_time', '1425284797'),
(52, 48, '_wp_trash_meta_status', 'publish'),
(53, 48, '_wp_trash_meta_time', '1425284797'),
(54, 66, '_edit_last', '1'),
(55, 66, '_edit_lock', '1425284815:1'),
(56, 66, '_thumbnail_id', '27'),
(57, 66, 'price', '$5000'),
(58, 68, '_edit_last', '1'),
(59, 68, '_edit_lock', '1425287142:1'),
(60, 68, '_thumbnail_id', '24'),
(61, 68, 'price', '$20'),
(62, 69, '_edit_last', '1'),
(63, 69, '_edit_lock', '1425287413:1'),
(64, 70, '_edit_last', '1'),
(65, 70, '_edit_lock', '1425446896:1'),
(66, 70, '_thumbnail_id', '27'),
(67, 71, '_edit_last', '1'),
(68, 71, '_edit_lock', '1425446905:1'),
(69, 71, '_thumbnail_id', '24'),
(70, 72, '_edit_last', '1'),
(71, 72, '_edit_lock', '1425447511:1'),
(72, 72, '_oembed_fd3e0171eed56ed736e70fa7a679357e', '<iframe width="660" height="371" src="https://www.youtube.com/embed/7mRQxS6Jpro?feature=oembed" frameborder="0" allowfullscreen></iframe>'),
(73, 72, '_oembed_time_fd3e0171eed56ed736e70fa7a679357e', '1425289184'),
(74, 72, '_thumbnail_id', '27'),
(75, 79, '_edit_last', '1'),
(76, 79, '_edit_lock', '1425455329:1'),
(77, 79, 'youtubeEmpbedVideoLink', 'http://www.youtube.com/embed/7mRQxS6Jpro?feature=oembed'),
(78, 81, '_edit_last', '1'),
(79, 81, '_edit_lock', '1425449357:1'),
(80, 81, '_thumbnail_id', '24');

-- --------------------------------------------------------

--
-- Table structure for table `wp7664786784_posts`
--

CREATE TABLE IF NOT EXISTS `wp7664786784_posts` (
`ID` bigint(20) unsigned NOT NULL,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84 ;

--
-- Dumping data for table `wp7664786784_posts`
--

INSERT INTO `wp7664786784_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2015-02-18 13:09:37', '2015-02-18 13:09:37', 'Welcome to WordPress. This is your first post. Edit or delete it, then start blogging!', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world', '', '', '2015-02-19 17:14:22', '2015-02-19 17:14:22', '', 0, 'http://localhost/rangemaster/?p=1', 0, 'post', '', 0),
(2, 1, '2015-02-18 13:09:37', '2015-02-18 13:09:37', 'This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my blog. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/rangemaster/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'trash', 'open', 'open', '', 'sample-page', '', '', '2015-02-19 16:19:54', '2015-02-19 16:19:54', '', 0, 'http://localhost/rangemaster/?page_id=2', 0, 'page', '', 0),
(4, 1, '2015-02-18 19:10:54', '2015-02-18 19:10:54', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla.\r\n\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.', 'About Us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2015-03-01 11:13:22', '2015-03-01 11:13:22', '', 0, 'http://localhost/rangemaster/?page_id=4', 0, 'page', '', 0),
(5, 1, '2015-02-18 19:10:54', '2015-02-18 19:10:54', '', 'about us', '', 'inherit', 'open', 'open', '', '4-revision-v1', '', '', '2015-02-18 19:10:54', '2015-02-18 19:10:54', '', 4, 'http://localhost/rangemaster/4-revision-v1/', 0, 'revision', '', 0),
(6, 1, '2015-02-19 16:07:13', '2015-02-19 16:07:13', '<p>Your Name (required)<br />\r\n    [text* your-name] </p>\r\n\r\n<p>Your Email (required)<br />\r\n    [email* your-email] </p>\r\n\r\n<p>Your Phone <br />\r\n    [text* your-phone] </p>\r\n\r\n<p>Your Message<br />\r\n    [textarea your-message] </p>\r\n\r\n<p>[submit "Send"]</p>\n[your-subject]\n[your-name] <hitendra.singh2007@gmail.com>\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n--\r\nThis e-mail was sent from a contact form on rangemaster (http://localhost/rangemaster)\nhitendra.singh2007@gmail.com\nReply-To: [your-email]\n\n\n\n\n[your-subject]\nrangemaster <hitendra.singh2007@gmail.com>\nMessage Body:\r\n[your-message]\r\n\r\n--\r\nThis e-mail was sent from a contact form on rangemaster (http://localhost/rangemaster)\n[your-email]\nReply-To: hitendra.singh2007@gmail.com\n\n\n\nYour message was sent successfully. Thanks.\nFailed to send your message. Please try later or contact the administrator by another method.\nValidation errors occurred. Please confirm the fields and submit it again.\nFailed to send your message. Please try later or contact the administrator by another method.\nPlease accept the terms to proceed.\nPlease fill in the required field.\nThis input is too long.\nThis input is too short.\nYour entered code is incorrect.\nNumber format seems invalid.\nThis number is too small.\nThis number is too large.\nEmail address seems invalid.\nURL seems invalid.\nTelephone number seems invalid.\nYour answer is not correct.\nDate format seems invalid.\nThis date is too early.\nThis date is too late.\nFailed to upload file.\nThis file type is not allowed.\nThis file is too large.\nFailed to upload file. Error occurred.', 'Contact form', '', 'publish', 'open', 'open', '', 'contact-form-1', '', '', '2015-02-19 16:10:52', '2015-02-19 16:10:52', '', 0, 'http://localhost/rangemaster/?post_type=wpcf7_contact_form&#038;p=6', 0, 'wpcf7_contact_form', '', 0),
(7, 1, '2015-02-19 16:11:35', '2015-02-19 16:11:35', '<a href="http://localhost/rangemaster/wp-content/uploads/2015/02/map.jpg"><img class="aligncenter size-full wp-image-9" src="http://localhost/rangemaster/wp-content/uploads/2015/02/map.jpg" alt="map" width="1231" height="401" /></a>\r\n\r\n[contact-form-7 id="6" title="Contact form"]\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa.', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2015-02-19 16:24:03', '2015-02-19 16:24:03', '', 0, 'http://localhost/rangemaster/?page_id=7', 0, 'page', '', 0),
(8, 1, '2015-02-19 16:11:35', '2015-02-19 16:11:35', '[contact-form-7 id="6" title="Contact form"]', 'Contact Us', '', 'inherit', 'open', 'open', '', '7-revision-v1', '', '', '2015-02-19 16:11:35', '2015-02-19 16:11:35', '', 7, 'http://localhost/rangemaster/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2015-02-19 16:12:59', '2015-02-19 16:12:59', '', 'map', '', 'inherit', 'open', 'open', '', 'map', '', '', '2015-02-19 16:12:59', '2015-02-19 16:12:59', '', 7, 'http://localhost/rangemaster/wp-content/uploads/2015/02/map.jpg', 0, 'attachment', 'image/jpeg', 0),
(10, 1, '2015-02-19 16:13:27', '2015-02-19 16:13:27', '<a href="http://localhost/rangemaster/wp-content/uploads/2015/02/map.jpg"><img class="aligncenter size-full wp-image-9" src="http://localhost/rangemaster/wp-content/uploads/2015/02/map.jpg" alt="map" width="1231" height="401" /></a>\r\n\r\n[contact-form-7 id="6" title="Contact form"]', 'Contact Us', '', 'inherit', 'open', 'open', '', '7-revision-v1', '', '', '2015-02-19 16:13:27', '2015-02-19 16:13:27', '', 7, 'http://localhost/rangemaster/7-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2015-02-19 16:14:09', '2015-02-19 16:14:09', '<a href="http://localhost/rangemaster/wp-content/uploads/2015/02/map.jpg"><img class="aligncenter size-full wp-image-9" src="http://localhost/rangemaster/wp-content/uploads/2015/02/map.jpg" alt="map" width="1231" height="401" /></a>\r\n\r\n[contact-form-7 id="6" title="Contact form"]', 'Contact', '', 'inherit', 'open', 'open', '', '7-revision-v1', '', '', '2015-02-19 16:14:09', '2015-02-19 16:14:09', '', 7, 'http://localhost/rangemaster/7-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2015-02-19 16:19:54', '2015-02-19 16:19:54', 'This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my blog. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/rangemaster/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'inherit', 'open', 'open', '', '2-revision-v1', '', '', '2015-02-19 16:19:54', '2015-02-19 16:19:54', '', 2, 'http://localhost/rangemaster/2-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2015-02-19 16:20:34', '2015-02-19 16:20:34', '', 'About Us', '', 'inherit', 'open', 'open', '', '4-revision-v1', '', '', '2015-02-19 16:20:34', '2015-02-19 16:20:34', '', 4, 'http://localhost/rangemaster/4-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2015-02-19 16:21:01', '2015-02-19 16:21:01', 'This is about us page.', 'About Us', '', 'inherit', 'open', 'open', '', '4-revision-v1', '', '', '2015-02-19 16:21:01', '2015-02-19 16:21:01', '', 4, 'http://localhost/rangemaster/4-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2015-02-19 16:24:03', '2015-02-19 16:24:03', '<a href="http://localhost/rangemaster/wp-content/uploads/2015/02/map.jpg"><img class="aligncenter size-full wp-image-9" src="http://localhost/rangemaster/wp-content/uploads/2015/02/map.jpg" alt="map" width="1231" height="401" /></a>\r\n\r\n[contact-form-7 id="6" title="Contact form"]\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa.', 'Contact', '', 'inherit', 'open', 'open', '', '7-revision-v1', '', '', '2015-02-19 16:24:03', '2015-02-19 16:24:03', '', 7, 'http://localhost/rangemaster/7-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2015-02-19 16:25:39', '2015-02-19 16:25:39', 'hello this is blog page.', 'Blog', '', 'publish', 'open', 'open', '', 'blog', '', '', '2015-03-04 08:28:44', '2015-03-04 08:28:44', '', 0, 'http://localhost/rangemaster/?page_id=16', 0, 'page', '', 0),
(17, 1, '2015-02-19 16:25:39', '2015-02-19 16:25:39', '', 'Blogs', '', 'inherit', 'open', 'open', '', '16-revision-v1', '', '', '2015-02-19 16:25:39', '2015-02-19 16:25:39', '', 16, 'http://localhost/rangemaster/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2015-02-19 16:25:45', '2015-02-19 16:25:45', '', 'Blog', '', 'inherit', 'open', 'open', '', '16-revision-v1', '', '', '2015-02-19 16:25:45', '2015-02-19 16:25:45', '', 16, 'http://localhost/rangemaster/16-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2015-02-19 16:29:29', '2015-02-19 16:29:29', '', 'Home', '', 'publish', 'open', 'open', '', 'home', '', '', '2015-02-19 16:30:12', '2015-02-19 16:30:12', '', 0, 'http://localhost/rangemaster/?page_id=19', 0, 'page', '', 0),
(20, 1, '2015-02-19 16:29:29', '2015-02-19 16:29:29', '', 'Home', '', 'inherit', 'open', 'open', '', '19-revision-v1', '', '', '2015-02-19 16:29:29', '2015-02-19 16:29:29', '', 19, 'http://localhost/rangemaster/19-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2015-02-19 17:14:22', '2015-02-19 17:14:22', 'Welcome to WordPress. This is your first post. Edit or delete it, then start blogging!', 'Hello world!', '', 'inherit', 'open', 'open', '', '1-revision-v1', '', '', '2015-02-19 17:14:22', '2015-02-19 17:14:22', '', 1, 'http://localhost/rangemaster/1-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2015-02-19 17:15:27', '2015-02-19 17:15:27', 'Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy', 'Blog', '', 'publish', 'open', 'open', '', 'blog', '', '', '2015-02-19 17:15:27', '2015-02-19 17:15:27', '', 0, 'http://localhost/rangemaster/?p=23', 0, 'post', '', 0),
(24, 1, '2015-02-19 17:15:15', '2015-02-19 17:15:15', 'starter second receipe', 'blog-1', '', 'inherit', 'open', 'open', '', 'blog-1', '', '', '2015-03-01 12:14:10', '2015-03-01 12:14:10', '', 23, 'http://localhost/rangemaster/wp-content/uploads/2015/02/blog-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(25, 1, '2015-02-19 17:15:27', '2015-02-19 17:15:27', 'Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy', 'Blog', '', 'inherit', 'open', 'open', '', '23-revision-v1', '', '', '2015-02-19 17:15:27', '2015-02-19 17:15:27', '', 23, 'http://localhost/rangemaster/23-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2015-02-19 17:16:06', '2015-02-19 17:16:06', 'Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy', 'Blog 2', '', 'publish', 'open', 'open', '', 'blog-2', '', '', '2015-02-19 17:16:06', '2015-02-19 17:16:06', '', 0, 'http://localhost/rangemaster/?p=26', 0, 'post', '', 0),
(27, 1, '2015-02-19 17:15:59', '2015-02-19 17:15:59', '', 'blog-2', '', 'inherit', 'open', 'open', '', 'blog-2', '', '', '2015-02-19 17:15:59', '2015-02-19 17:15:59', '', 26, 'http://localhost/rangemaster/wp-content/uploads/2015/02/blog-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(28, 1, '2015-02-19 17:16:06', '2015-02-19 17:16:06', 'Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy Blog content dummy', 'Blog 2', '', 'inherit', 'open', 'open', '', '26-revision-v1', '', '', '2015-02-19 17:16:06', '2015-02-19 17:16:06', '', 26, 'http://localhost/rangemaster/26-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2015-03-01 07:11:34', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 07:11:34', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?p=29', 0, 'post', '', 0),
(30, 1, '2015-03-01 08:27:12', '2015-03-01 08:27:12', 'This is about us page._', 'About Us', '', 'inherit', 'open', 'open', '', '4-revision-v1', '', '', '2015-03-01 08:27:12', '2015-03-01 08:27:12', '', 4, 'http://localhost/rangemaster/4-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2015-03-01 08:39:24', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 08:39:24', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?page_id=31', 0, 'page', '', 0),
(32, 1, '2015-03-01 08:39:41', '2015-03-01 08:39:41', 'My Recipies this is demo', 'My Recipies', '', 'publish', 'open', 'open', '', 'my-recipies', '', '', '2015-03-01 08:39:41', '2015-03-01 08:39:41', '', 0, 'http://localhost/rangemaster/?page_id=32', 0, 'page', '', 0),
(33, 1, '2015-03-01 08:39:41', '2015-03-01 08:39:41', 'My Recipies this is demo', 'My Recipies', '', 'inherit', 'open', 'open', '', '32-revision-v1', '', '', '2015-03-01 08:39:41', '2015-03-01 08:39:41', '', 32, 'http://localhost/rangemaster/32-revision-v1/', 0, 'revision', '', 0),
(34, 1, '2015-03-01 08:45:05', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 08:45:05', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?page_id=34', 0, 'page', '', 0),
(35, 1, '2015-03-01 08:45:05', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 08:45:05', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?page_id=35', 0, 'page', '', 0),
(36, 1, '2015-03-01 08:45:27', '2015-03-01 08:45:27', 'awards this is awards demo page', 'awards', '', 'publish', 'open', 'open', '', 'awards', '', '', '2015-03-01 08:45:27', '2015-03-01 08:45:27', '', 0, 'http://localhost/rangemaster/?page_id=36', 0, 'page', '', 0),
(37, 1, '2015-03-01 08:45:14', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 08:45:14', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?page_id=37', 0, 'page', '', 0),
(38, 1, '2015-03-01 08:45:27', '2015-03-01 08:45:27', 'awards this is awards demo page', 'awards', '', 'inherit', 'open', 'open', '', '36-revision-v1', '', '', '2015-03-01 08:45:27', '2015-03-01 08:45:27', '', 36, 'http://localhost/rangemaster/36-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2015-03-01 08:46:12', '2015-03-01 08:46:12', 'hello this is blog page.', 'Blog', '', 'inherit', 'open', 'open', '', '16-autosave-v1', '', '', '2015-03-01 08:46:12', '2015-03-01 08:46:12', '', 16, 'http://localhost/rangemaster/16-autosave-v1/', 0, 'revision', '', 0),
(40, 1, '2015-03-01 08:46:15', '2015-03-01 08:46:15', 'hello this is blog page.', 'Blog', '', 'inherit', 'open', 'open', '', '16-revision-v1', '', '', '2015-03-01 08:46:15', '2015-03-01 08:46:15', '', 16, 'http://localhost/rangemaster/16-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2015-03-01 08:47:56', '2015-03-01 08:47:56', 'hello this is news demo page.', 'News', '', 'publish', 'open', 'open', '', 'news', '', '', '2015-03-01 08:47:56', '2015-03-01 08:47:56', '', 0, 'http://localhost/rangemaster/?page_id=41', 0, 'page', '', 0),
(42, 1, '2015-03-01 08:47:56', '2015-03-01 08:47:56', 'hello this is news demo page.', 'News', '', 'inherit', 'open', 'open', '', '41-revision-v1', '', '', '2015-03-01 08:47:56', '2015-03-01 08:47:56', '', 41, 'http://localhost/rangemaster/41-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2015-03-01 11:02:04', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 11:02:04', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?page_id=43', 0, 'page', '', 0),
(44, 1, '2015-03-01 11:02:06', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 11:02:06', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?page_id=44', 0, 'page', '', 0),
(45, 1, '2015-03-01 11:13:22', '2015-03-01 11:13:22', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla.\r\n\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.', 'About Us', '', 'inherit', 'open', 'open', '', '4-revision-v1', '', '', '2015-03-01 11:13:22', '2015-03-01 11:13:22', '', 4, 'http://localhost/rangemaster/4-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2015-03-01 12:06:12', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 12:06:12', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=46', 0, 'my_recepies', '', 0),
(47, 1, '2015-03-01 12:08:31', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 12:08:31', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=47', 0, 'my_recepies', '', 0),
(48, 1, '2015-03-01 12:10:21', '2015-03-01 12:10:21', 'this is starter recipe', 'starter recipe one', '', 'trash', 'closed', 'closed', '', 'starter-recipe-one', '', '', '2015-03-02 08:26:37', '2015-03-02 08:26:37', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&#038;p=48', 0, 'my_recepies', '', 0),
(49, 1, '2015-03-01 12:09:24', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 12:09:24', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=49', 0, 'my_recepies', '', 0),
(50, 1, '2015-03-01 12:10:47', '2015-03-01 12:10:47', 'this is starter recipe', 'starter recipe one', '', 'inherit', 'open', 'open', '', '48-autosave-v1', '', '', '2015-03-01 12:10:47', '2015-03-01 12:10:47', '', 48, 'http://localhost/rangemaster/48-autosave-v1/', 0, 'revision', '', 0),
(51, 1, '2015-03-01 12:12:51', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 12:12:51', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=51', 0, 'my_recepies', '', 0),
(52, 1, '2015-03-01 12:14:14', '2015-03-01 12:14:14', 'this is second receipe description', 'second receipe second', '', 'publish', 'closed', 'closed', '', 'second-receipe', '', '', '2015-03-01 12:14:34', '2015-03-01 12:14:34', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&#038;p=52', 0, 'my_recepies', '', 0),
(53, 1, '2015-03-01 14:33:33', '0000-00-00 00:00:00', 'lasdjfk', 'another receipe', '', 'draft', 'closed', 'closed', '', '', '', '', '2015-03-01 14:33:33', '2015-03-01 14:33:33', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&#038;p=53', 0, 'my_recepies', '', 0),
(54, 1, '2015-03-01 14:40:55', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 14:40:55', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=54', 0, 'my_recepies', '', 0),
(55, 1, '2015-03-01 14:45:05', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 14:45:05', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=55', 0, 'my_recepies', '', 0),
(56, 1, '2015-03-01 14:45:20', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 14:45:20', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=56', 0, 'my_recepies', '', 0),
(57, 1, '2015-03-01 14:45:34', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 14:45:34', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=57', 0, 'my_recepies', '', 0),
(58, 1, '2015-03-01 14:45:43', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 14:45:43', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=58', 0, 'my_recepies', '', 0),
(59, 1, '2015-03-01 14:46:04', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 14:46:04', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=59', 0, 'my_recepies', '', 0),
(60, 1, '2015-03-01 14:46:05', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 14:46:05', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=60', 0, 'my_recepies', '', 0),
(61, 1, '2015-03-01 14:46:05', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 14:46:05', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=61', 0, 'my_recepies', '', 0),
(62, 1, '2015-03-01 14:53:52', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 14:53:52', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=62', 0, 'my_recepies', '', 0),
(63, 1, '2015-03-01 14:53:59', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 14:53:59', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=awards&p=63', 0, 'awards', '', 0),
(64, 1, '2015-03-01 14:54:17', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-01 14:54:17', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=awards&p=64', 0, 'awards', '', 0),
(65, 1, '2015-03-02 08:28:08', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-02 08:28:08', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=65', 0, 'my_recepies', '', 0),
(66, 1, '2015-03-02 08:28:57', '2015-03-02 08:28:57', 'hello this is lunch', 'First lunch', '', 'publish', 'closed', 'closed', '', 'first-lunch', '', '', '2015-03-02 08:28:57', '2015-03-02 08:28:57', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&#038;p=66', 0, 'my_recepies', '', 0),
(67, 1, '2015-03-02 08:29:19', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-02 08:29:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&p=67', 0, 'my_recepies', '', 0),
(68, 1, '2015-03-02 08:30:14', '2015-03-02 08:30:14', 'adsfasdfasdf', 'second lunch', '', 'publish', 'closed', 'closed', '', 'second-lunch', '', '', '2015-03-02 08:30:14', '2015-03-02 08:30:14', '', 0, 'http://localhost/rangemaster/?post_type=my_recepies&#038;p=68', 0, 'my_recepies', '', 0),
(69, 1, '2015-03-02 09:10:13', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2015-03-02 09:10:13', '2015-03-02 09:10:13', '', 0, 'http://localhost/rangemaster/?post_type=awards&#038;p=69', 0, 'awards', '', 0),
(70, 1, '2015-03-02 09:12:32', '2015-03-02 09:12:32', 'hello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demohello first award demo', 'first award', '', 'publish', 'closed', 'closed', '', 'first-award', '', '', '2015-03-02 09:13:09', '2015-03-02 09:13:09', '', 0, 'http://localhost/rangemaster/?post_type=awards&#038;p=70', 0, 'awards', '', 0),
(71, 1, '2015-03-02 09:12:55', '2015-03-02 09:12:55', 'hello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second awardhello this is second award', 'second award', '', 'publish', 'closed', 'closed', '', 'second', '', '', '2015-03-02 09:13:27', '2015-03-02 09:13:27', '', 0, 'http://localhost/rangemaster/?post_type=awards&#038;p=71', 0, 'awards', '', 0),
(72, 1, '2015-03-02 09:39:56', '2015-03-02 09:39:56', 'third awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird awardthird award', 'third award', '', 'publish', 'closed', 'closed', '', 'third-award', '', '', '2015-03-04 05:38:27', '2015-03-04 05:38:27', '', 0, 'http://localhost/rangemaster/?post_type=awards&#038;p=72', 0, 'awards', '', 0),
(73, 1, '2015-03-02 09:44:58', '2015-03-02 09:44:58', '', 'third award', '', 'inherit', 'open', 'open', '', '72-autosave-v1', '', '', '2015-03-02 09:44:58', '2015-03-02 09:44:58', '', 72, 'http://localhost/rangemaster/72-autosave-v1/', 0, 'revision', '', 0),
(74, 1, '2015-03-02 09:59:08', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-02 09:59:08', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=news&p=74', 0, 'news', '', 0),
(75, 1, '2015-03-02 10:01:09', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-02 10:01:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=news&p=75', 0, 'news', '', 0),
(76, 1, '2015-03-02 10:01:48', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-02 10:01:48', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=news&p=76', 0, 'news', '', 0),
(77, 1, '2015-03-04 05:43:26', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-04 05:43:26', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?p=77', 0, 'post', '', 0),
(78, 1, '2015-03-04 06:08:10', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-04 06:08:10', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=news&p=78', 0, 'news', '', 0),
(79, 1, '2015-03-04 06:11:01', '2015-03-04 06:11:01', 'hello this is first new description', 'First News', '', 'publish', 'closed', 'closed', '', 'first-news', '', '', '2015-03-04 06:11:01', '2015-03-04 06:11:01', '', 0, 'http://localhost/rangemaster/?post_type=news&#038;p=79', 0, 'news', '', 0),
(80, 1, '2015-03-04 06:11:03', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-04 06:11:03', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=news&p=80', 0, 'news', '', 0),
(81, 1, '2015-03-04 06:11:39', '2015-03-04 06:11:39', 'Hello this is second new description.', 'Second new', '', 'publish', 'closed', 'closed', '', 'second-new', '', '', '2015-03-04 06:11:39', '2015-03-04 06:11:39', '', 0, 'http://localhost/rangemaster/?post_type=news&#038;p=81', 0, 'news', '', 0),
(82, 1, '2015-03-04 06:28:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-04 06:28:02', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=news&p=82', 0, 'news', '', 0),
(83, 1, '2015-03-04 06:28:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-03-04 06:28:02', '0000-00-00 00:00:00', '', 0, 'http://localhost/rangemaster/?post_type=news&p=83', 0, 'news', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp7664786784_terms`
--

CREATE TABLE IF NOT EXISTS `wp7664786784_terms` (
`term_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `wp7664786784_terms`
--

INSERT INTO `wp7664786784_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'starter', 'starter', 0),
(3, 'lunch', 'lunch', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp7664786784_term_relationships`
--

CREATE TABLE IF NOT EXISTS `wp7664786784_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp7664786784_term_relationships`
--

INSERT INTO `wp7664786784_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(23, 1, 0),
(26, 1, 0),
(48, 2, 0),
(52, 2, 0),
(66, 3, 0),
(68, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp7664786784_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `wp7664786784_term_taxonomy` (
`term_taxonomy_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `wp7664786784_term_taxonomy`
--

INSERT INTO `wp7664786784_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 2),
(2, 2, 'category', 'this is starter description', 0, 1),
(3, 3, 'category', 'are you hungry', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wp7664786784_usermeta`
--

CREATE TABLE IF NOT EXISTS `wp7664786784_usermeta` (
`umeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `wp7664786784_usermeta`
--

INSERT INTO `wp7664786784_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wp7664786784_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'wp7664786784_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets,wp410_dfw'),
(13, 1, 'show_welcome_panel', '1'),
(14, 1, 'session_tokens', 'a:3:{s:64:"2c56cdce534d988f37db23df3cbadf6ee93dcad389c00fbdb3b7da93824c261d";a:4:{s:10:"expiration";i:1425474600;s:2:"ip";s:3:"::1";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.65 Safari/537.36";s:5:"login";i:1424265000;}s:64:"e70f73f086b3b2d24c2672032db06cbd0a90c29fc7b42c702dc1c8149eab7cc6";a:4:{s:10:"expiration";i:1425366690;s:2:"ip";s:3:"::1";s:2:"ua";s:109:"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36";s:5:"login";i:1425193890;}s:64:"4b44d9e0e08dd404353e8067f72fc8b396a1f7b117806e3953179472be8b1b03";a:4:{s:10:"expiration";i:1426429809;s:2:"ip";s:3:"::1";s:2:"ua";s:109:"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36";s:5:"login";i:1425220209;}}'),
(15, 1, 'wp7664786784_dashboard_quick_press_last_post_id', '29'),
(16, 1, 'closedpostboxes_toplevel_page_wpcf7', 'a:0:{}'),
(17, 1, 'metaboxhidden_toplevel_page_wpcf7', 'a:0:{}'),
(18, 1, 'wp7664786784_user-settings', 'editor=html&libraryContent=browse&imgsize=full&align=center'),
(19, 1, 'wp7664786784_user-settings-time', '1425289561'),
(20, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";}'),
(21, 1, 'metaboxhidden_nav-menus', 'a:3:{i:0;s:8:"add-post";i:1;s:12:"add-post_tag";i:2;s:15:"add-post_format";}'),
(22, 1, 'closedpostboxes_page', 'a:0:{}'),
(23, 1, 'metaboxhidden_page', 'a:3:{i:0;s:12:"revisionsdiv";i:1;s:10:"postcustom";i:2;s:7:"slugdiv";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp7664786784_users`
--

CREATE TABLE IF NOT EXISTS `wp7664786784_users` (
`ID` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(64) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT ''
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `wp7664786784_users`
--

INSERT INTO `wp7664786784_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BpN0RVMh0FX28eFjuQbpCs8Ljagv.J.', 'admin', 'hitendra.singh2007@gmail.com', '', '2015-02-18 13:09:36', '', 0, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp7664786784_commentmeta`
--
ALTER TABLE `wp7664786784_commentmeta`
 ADD PRIMARY KEY (`meta_id`), ADD KEY `comment_id` (`comment_id`), ADD KEY `meta_key` (`meta_key`);

--
-- Indexes for table `wp7664786784_comments`
--
ALTER TABLE `wp7664786784_comments`
 ADD PRIMARY KEY (`comment_ID`), ADD KEY `comment_post_ID` (`comment_post_ID`), ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`), ADD KEY `comment_date_gmt` (`comment_date_gmt`), ADD KEY `comment_parent` (`comment_parent`), ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp7664786784_links`
--
ALTER TABLE `wp7664786784_links`
 ADD PRIMARY KEY (`link_id`), ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp7664786784_options`
--
ALTER TABLE `wp7664786784_options`
 ADD PRIMARY KEY (`option_id`), ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp7664786784_postmeta`
--
ALTER TABLE `wp7664786784_postmeta`
 ADD PRIMARY KEY (`meta_id`), ADD KEY `post_id` (`post_id`), ADD KEY `meta_key` (`meta_key`);

--
-- Indexes for table `wp7664786784_posts`
--
ALTER TABLE `wp7664786784_posts`
 ADD PRIMARY KEY (`ID`), ADD KEY `post_name` (`post_name`), ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`), ADD KEY `post_parent` (`post_parent`), ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp7664786784_terms`
--
ALTER TABLE `wp7664786784_terms`
 ADD PRIMARY KEY (`term_id`), ADD KEY `slug` (`slug`), ADD KEY `name` (`name`);

--
-- Indexes for table `wp7664786784_term_relationships`
--
ALTER TABLE `wp7664786784_term_relationships`
 ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`), ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp7664786784_term_taxonomy`
--
ALTER TABLE `wp7664786784_term_taxonomy`
 ADD PRIMARY KEY (`term_taxonomy_id`), ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`), ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp7664786784_usermeta`
--
ALTER TABLE `wp7664786784_usermeta`
 ADD PRIMARY KEY (`umeta_id`), ADD KEY `user_id` (`user_id`), ADD KEY `meta_key` (`meta_key`);

--
-- Indexes for table `wp7664786784_users`
--
ALTER TABLE `wp7664786784_users`
 ADD PRIMARY KEY (`ID`), ADD KEY `user_login_key` (`user_login`), ADD KEY `user_nicename` (`user_nicename`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp7664786784_commentmeta`
--
ALTER TABLE `wp7664786784_commentmeta`
MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp7664786784_comments`
--
ALTER TABLE `wp7664786784_comments`
MODIFY `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp7664786784_links`
--
ALTER TABLE `wp7664786784_links`
MODIFY `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp7664786784_options`
--
ALTER TABLE `wp7664786784_options`
MODIFY `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=281;
--
-- AUTO_INCREMENT for table `wp7664786784_postmeta`
--
ALTER TABLE `wp7664786784_postmeta`
MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `wp7664786784_posts`
--
ALTER TABLE `wp7664786784_posts`
MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `wp7664786784_terms`
--
ALTER TABLE `wp7664786784_terms`
MODIFY `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wp7664786784_term_taxonomy`
--
ALTER TABLE `wp7664786784_term_taxonomy`
MODIFY `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wp7664786784_usermeta`
--
ALTER TABLE `wp7664786784_usermeta`
MODIFY `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `wp7664786784_users`
--
ALTER TABLE `wp7664786784_users`
MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
