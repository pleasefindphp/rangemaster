<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">


	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<script>(function(){document.documentElement.className='js'})();</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="main">
      <div class="wrapper">
        <span class="corner-top-left"></span>
        <span class="corner-top-right"></span>

        <header>
          <div class="container">
            <div class="row">
              <div class="col-xs-12  col-sm-6  col-sm-push-6  col-md-4  col-md-push-0">
                <div class="header-title">
                Phone: +1800 12 3456 78 <p class="header-subtitle">Feel free to call us</p>
                </div>
              </div>
              <div class="col-xs-12  col-sm-6  col-sm-pull-6  col-md-4  col-md-pull-0">
                <div class="header-logo">
                <!-- Logo start -->
                <a href="#">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" alt="">
                </a>
                <!-- Logo end -->
                </div>
              </div>
              <div class="col-xs-12  col-md-4">
                <div class="header-social  clearfix">
                  <a href="#" target="_blank" class="social-container"> <span class="fa  fa-lg  fa-facebook"></span></a>
                  <a href="#/" target="_blank" class="social-container"> <span class="fa  fa-lg  fa-rss"></span></a>
                  <a href="#" target="_blank" class="social-container"> <span class="fa  fa-lg  fa-twitter"></span></a>
                  <a href="#" target="_blank" class="social-container"> <span class="fa  fa-lg  fa-youtube"></span></a>
                </div>
              </div>
            </div>
          </div>
        </header>


        <nav class="navbar">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li class="active child">
                  <a href="<?php echo home_url();?>">Home</a>
                  <!-- <ul class="sub-menu">
                    <li><a href="#">Menu 1</a></li>
                    <li><a href="#">Menu 2</a></li>
                    <li><a href="#">Menu 3</a></li>
                  </ul> -->
                </li>
                <li><a href="<?php echo home_url();?>/about-us">About Us</a></li>
                <li><a href="<?php echo home_url();?>/my-recipies">My Recipies</a></li>
                <li><a href="<?php echo home_url();?>/awards">Awards</a></li>
                <li><a href="<?php echo home_url();?>/blog">Blog</a></li>
                <li><a href="<?php echo home_url();?>/news">News</a></li>
                <li><a href="<?php echo home_url();?>/contact">Contact</a></li>
              </ul>
              <a href="<?php echo home_url();?>/contact" class="btn btn-contact  pull-right">Contact Me</a>
            </div><!--/.nav-collapse -->
          </div>
        </nav>