<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			// get_template_part( 'content', 'page' );
		?>
		<!-- template design -->
		<div class="container">
                <div class="row">
                    <div class="title-area">
                        <div class="container">
                            <h1>About Us</h1>
                        </div>
                    </div>
                    <div class="breadcrumbs-container">
                        <div class="container breadcrumb">
                            <!-- Breadcrumb NavXT 5.2.0 -->
                            <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to Dining Restaurant." href="#/" class="home">Dining Restaurant</a></span><span class="separator">&gt;</span><span typeof="v:Breadcrumb"><span property="v:title">About Us</span></span>
                        </div>
                        <div class="container">
                            <div class="row">
                                <p class="font-text-large"><?php the_title();  ?></p>
                                <p><?php the_content(); ?></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- template design -->

		<?php 
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		// End the loop.
		endwhile;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
