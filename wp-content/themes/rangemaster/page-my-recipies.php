<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
    <!-- my recipe design -->
    <div class="container">
        <div class="row">
            <div class="title-area">
                <div class="container">
                    <h1>MY RECIPIES</h1>
                </div>
            </div>
            <div class="breadcrumbs-container">
                <div class="container breadcrumb">
                    <!-- Breadcrumb NavXT 5.2.0 -->
                    <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to Dining Restaurant." href="#/" class="home">Dining Restaurant</a></span><span class="separator">&gt;</span><span typeof="v:Breadcrumb"><span property="v:title">Recipies</span></span>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12  col-md-12" role="main">
                            <article class="post-1225 page type-page status-publish">
                                <div class="hentry__content">
                                    <p><span style="color: #444444;"></span>
                                    </p>
                                    <ul id="fdm-menu-1" class="fdm-menu fdm-menu-1294 fdm-columns-2 fdm-layout-classic clearfix">
                                        <?php 
                                            $taxonomy = 'category';
                                            $terms = get_terms($taxonomy);

                                            if ( $terms && !is_wp_error( $terms ) ) :
                                                foreach ($terms as $term) {
                                                    if( $term->slug != 'uncategorized' ){
                                                    $loop = new WP_Query( array( 
                                                        'post_type' => 'my_recepies', 
                                                        'category_name' => $term->slug, 
                                                    ) );
                                        ?>
                                        <li class="fdm-column fdm-column-0">
                                            <ul class="fdm-section fdm-sectionid-37 fdm-section-0">
                                                <li class="fdm-section-header">
                                                    <h3><?php echo $term->name; ?></h3>
                                                    <p><?php echo $term->description ?></p>
                                                </li>
                                                <?php
                                                    while ( $loop->have_posts() ) : $loop->the_post();  
                                                ?>
                                                <li class="fdm-item fdm-item-has-image fdm-item-has-price">
                                                    <div class="fdm-item-panel">
                                                        <!-- image -->
                                                        <?php 
                                                            if (has_post_thumbnail()) {
                                                                $attrs = array( 
                                                                    "class" =>"fdm-item-image", 
                                                                    "title" => the_title(), 
                                                                    "alt" => the_title(),
                                                                    "pagespeed_url_hash" =>"506116753", 
                                                                    "onload" =>"pagespeed.CriticalImages.checkImageForCriticality(this)", 
                                                                    "width" =>"600", 
                                                                    "height" =>"600"
                                                                );
                                                                the_post_thumbnail( 'thumbnail', $attrs );    
                                                            }
                                                        ?>
                                                        <p class="fdm-item-title"><?php the_title(); ?></p>
                                                        <div class="fdm-item-price-wrapper">
                                                            <span class="fdm-item-price"><?php echo get_post_custom_values('price')[0]; ?></span>
                                                        </div>
                                                        <div class="fdm-item-content">
                                                            <p><?php echo the_content(); ?></p>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </li>
                                                <?php endwhile;  ?>
                                                </ul>
                                                </li>
                                        <?php
                                                }
                                                }
                                            endif;
                                        ?>
                                            </ul>
                                        
                                    </ul>
                                    <p></p>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>

            </div>
        </div>
            </div>
    <!-- my recipe design -->

<?php get_footer(); ?>
