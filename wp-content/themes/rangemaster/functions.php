<?php
/**
 * Twenty Fifteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Twenty Fifteen 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

/**
 * Twenty Fifteen only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'rangemaster_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Twenty Fifteen 1.0
 */
function rangemaster_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on rangemaster, use a find and replace
	 * to change 'rangemaster' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'rangemaster', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'rangemaster' ),
		'social'  => __( 'Social Links Menu', 'rangemaster' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	// $color_scheme  = rangemaster_get_color_scheme();
	// $default_color = trim( $color_scheme[0], '#' );

	// // Setup the WordPress core custom background feature.
	// add_theme_support( 'custom-background', apply_filters( 'rangemaster_custom_background_args', array(
	// 	'default-color'      => $default_color,
	// 	'default-attachment' => 'fixed',
	// ) ) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', rangemaster_fonts_url() ) );
}
endif; // rangemaster_setup
add_action( 'after_setup_theme', 'rangemaster_setup' );

/**
 * Register widget area.
 *
 * @since Twenty Fifteen 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function rangemaster_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'rangemaster' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'rangemaster' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'rangemaster_widgets_init' );

if ( ! function_exists( 'rangemaster_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Fifteen.
 *
 * @since Twenty Fifteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function rangemaster_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Noto Sans, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'rangemaster' ) ) {
		$fonts[] = 'Noto Sans:400italic,700italic,400,700';
	}

	/* translators: If there are characters in your language that are not supported by Noto Serif, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'rangemaster' ) ) {
		$fonts[] = 'Noto Serif:400italic,700italic,400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'rangemaster' ) ) {
		$fonts[] = 'Inconsolata:400,700';
	}

	/* translators: To add an additional character subset specific to your language, translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language. */
	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'rangemaster' );

	if ( 'cyrillic' == $subset ) {
		$subsets .= ',cyrillic,cyrillic-ext';
	} elseif ( 'greek' == $subset ) {
		$subsets .= ',greek,greek-ext';
	} elseif ( 'devanagari' == $subset ) {
		$subsets .= ',devanagari';
	} elseif ( 'vietnamese' == $subset ) {
		$subsets .= ',vietnamese';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), '//fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Fifteen 1.0
 */
function rangemaster_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'rangemaster-fonts', rangemaster_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

	// Load our main stylesheet.

	wp_enqueue_style( 'rangemaster-style', get_stylesheet_uri() );

	// Load our custom stylesheets.

	wp_enqueue_style( 'rangemaster-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array( 'rangemaster-style' ), '20141010' );
	wp_enqueue_style( 'rangemaster-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array( 'rangemaster-style' ), '20141010' );
	wp_enqueue_style( 'rangemaster-stylecustom', get_template_directory_uri() . '/css/style.css', array( 'rangemaster-style' ), '20141010' );
	wp_enqueue_style( 'rangemaster-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array( 'rangemaster-style' ), '20141010' );
	
	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'rangemaster-ie', get_template_directory_uri() . '/css/ie.css', array( 'rangemaster-style' ), '20141010' );
	wp_style_add_data( 'rangemaster-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'rangemaster-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'rangemaster-style' ), '20141010' );
	wp_style_add_data( 'rangemaster-ie7', 'conditional', 'lt IE 8' );

	wp_enqueue_script( 'rangemaster-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'rangemaster-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
	}

	wp_enqueue_script( 'rangemaster-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20141212', true );
	wp_enqueue_script( 'rangemaster-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '20141212', true );
	wp_localize_script( 'rangemaster-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'rangemaster' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'rangemaster' ) . '</span>',
	) );
}
add_action( 'wp_enqueue_scripts', 'rangemaster_scripts' );

/**
 * Add featured image as background image to post navigation elements.
 *
 * @since Twenty Fifteen 1.0
 *
 * @see wp_add_inline_style()
 */
function rangemaster_post_nav_background() {
	if ( ! is_single() ) {
		return;
	}

	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );
	$css      = '';

	if ( is_attachment() && 'attachment' == $previous->post_type ) {
		return;
	}

	if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
		$prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-previous { background-image: url(' . esc_url( $prevthumb[0] ) . '); }
			.post-navigation .nav-previous .post-title, .post-navigation .nav-previous a:hover .post-title, .post-navigation .nav-previous .meta-nav { color: #fff; }
			.post-navigation .nav-previous a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	if ( $next && has_post_thumbnail( $next->ID ) ) {
		$nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-next { background-image: url(' . esc_url( $nextthumb[0] ) . '); }
			.post-navigation .nav-next .post-title, .post-navigation .nav-next a:hover .post-title, .post-navigation .nav-next .meta-nav { color: #fff; }
			.post-navigation .nav-next a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	wp_add_inline_style( 'rangemaster-style', $css );
}
add_action( 'wp_enqueue_scripts', 'rangemaster_post_nav_background' );

/**
 * Display descriptions in main navigation.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string  $item_output The menu item output.
 * @param WP_Post $item        Menu item object.
 * @param int     $depth       Depth of the menu.
 * @param array   $args        wp_nav_menu() arguments.
 * @return string Menu item with possible description.
 */
function rangemaster_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'rangemaster_nav_description', 10, 4 );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function rangemaster_search_form_modify( $html ) {
	return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
add_filter( 'get_search_form', 'rangemaster_search_form_modify' );

/**
 * Implement the Custom Header feature.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/customizer.php';

/*-------------------------------------------------------------------------------------------*/
/* my_recepies Post Type */
/*-------------------------------------------------------------------------------------------*/
class my_recepies {

	function my_recepies() {
		add_action('init',array($this,'create_post_type'));
	}

	function create_post_type() {
		$labels = array(
		    'name' => 'Recepies',
		    'singular_name' => 'Recipe',
		    'add_new' => 'Add New',
		    'all_items' => 'All Recepies',
		    'add_new_item' => 'Add New Recipe',
		    'edit_item' => 'Edit Recipe',
		    'new_item' => 'New Recipe',
		    'view_item' => 'View Recipe',
		    'search_items' => 'Search Recipies',
		    'not_found' =>  'No Recipies found',
		    'not_found_in_trash' => 'No Recipies found in trash',
		    'parent_item_colon' => 'Parent Recipe:',
		    'menu_name' => 'Recipies'
		);
		$args = array(
			'labels' => $labels,
			'description' => "A description for your recipies ",
			'public' => true,
			'taxonomies' => array('category'),
			'exclude_from_search' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 20,
			'menu_icon' => null,
			'capability_type' => 'post',
			'hierarchical' => true,
			'supports' => array('title','editor','thumbnail', 'custom-fields'),
			'has_archive' => true,
			'rewrite' => false,
			'query_var' => true,
			'can_export' => true
		);
		register_post_type('my_recepies',$args);
	}
}

$my_recepies = new my_recepies();

/*-------------------------------------------------------------------------------------------*/
/* awards Post Type */
/*-------------------------------------------------------------------------------------------*/
class awards {

	function awards() {
		add_action('init',array($this,'create_post_type'));
	}

	function create_post_type() {
		$labels = array(
		    'name' => 'Awards',
		    'singular_name' => 'Award',
		    'add_new' => 'Add New',
		    'all_items' => 'All Awards',
		    'add_new_item' => 'Add New Award',
		    'edit_item' => 'Edit Award',
		    'new_item' => 'New Award',
		    'view_item' => 'View Award',
		    'search_items' => 'Search Award',
		    'not_found' =>  'No Award found',
		    'not_found_in_trash' => 'No Awards found in trash',
		    'parent_item_colon' => 'Parent Award:',
		    'menu_name' => 'Award'
		);
		$args = array(
			'labels' => $labels,
			'description' => "A description for your awards",
			'public' => true,
			'exclude_from_search' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 20,
			'menu_icon' => null,
			'capability_type' => 'post',
			'hierarchical' => true,
			'supports' => array('title','editor','thumbnail'),
			'has_archive' => true,
			'rewrite' => false,
			'query_var' => true,
			'can_export' => true
		);
		register_post_type('awards',$args);
	}
}

$awards = new awards();

/*-------------------------------------------------------------------------------------------*/
/* news Post Type */
/*-------------------------------------------------------------------------------------------*/
class news {

	function news() {
		add_action('init',array($this,'create_post_type'));
	}

	function create_post_type() {
		$labels = array(
		    'name' => 'Newses',
		    'singular_name' => 'News',
		    'add_new' => 'Add New',
		    'all_items' => 'All Newes',
		    'add_new_item' => 'Add New News',
		    'edit_item' => 'Edit News',
		    'new_item' => 'New News',
		    'view_item' => 'View News',
		    'search_items' => 'Search Newes',
		    'not_found' =>  'No Newes found',
		    'not_found_in_trash' => 'No Newes found in trash',
		    'parent_item_colon' => 'Parent Newes:',
		    'menu_name' => 'Newes'
		);
		$args = array(
			'labels' => $labels,
			'description' => "A description for your news type",
			'public' => true,
			'exclude_from_search' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 20,
			'menu_icon' => null,
			'capability_type' => 'post',
			'hierarchical' => true,
			'supports' => array('title','editor', 'thumbnail', 'custom-fields'),
			'has_archive' => true,
			'rewrite' => false,
			'query_var' => true,
			'can_export' => true
		);
		register_post_type('news',$args);
	}
}

$news = new news();
																																