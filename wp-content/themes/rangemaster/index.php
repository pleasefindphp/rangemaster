<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <!-- <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol> -->

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="<?php echo get_template_directory_uri(); ?>/images/slider.jpg" alt="">
              <div class="carousel-caption">
              </div>
            </div>
          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

        <div class="divide-line">
          <div class="icon icons-divider-0"></div>
        </div>
        

        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <a href="#" class="btn  btn-read  pull-right">Read more</a>
              <h3 class="title">
                <span class="title-primary">
                <span class="light">About</span> Us</span><span class="title-secondary">A little something about who we are</span>
              </h3> 
              <div class="textwidget"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. </p>
              <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.</p>
              </div>
            </div>
            <div class="col-md-6">
              <h3 class="title">
                <span class="title-primary">
                <span class="light">Gorden</span> ramsey</span><span class="title-secondary">Youtube Video</span>
              </h3>
              <div class="textwidget">
                <img src="<?php echo get_template_directory_uri(); ?>/images/youtube.jpg" alt="">
              </div>
            </div>
          </div>


          <div class="gallery-bg">
            <div class="events">
              <div class="title-center">
                <h3 class="widget-title"><span class="light">UPCOMING</span> EVENTS</h3>  
              </div>
              <div class="row">
                <div class="col-xs-12  col-sm-6  col-md-3">
                  <div class="widget-single-event">
                    <div class="picture  push-down-15">
                      <a href="#/">
                        <img width="263" height="180" src="<?php echo get_template_directory_uri(); ?>/images/event-1.jpg" class="img-responsive" alt="healthy_breakfast"> 
                        <span class="img-overlay">
                          <span class="btn">Read more</span>
                        </span>
                      </a>
                    </div>
                    <h3 class="widget-single-event-title  no-margin"><a href="#"><span class="light">Healthy</span> Breakfast</a></h3>
                    <time class="widget-single-event-date">
                      <span class="entry-date date"><a href="#/" rel="bookmark"><abbr class="dtstart" title="2015-07-08 8:00"></abbr><abbr class="dtend" title="2015-07-08 11:00"></abbr>July 8, 2015 at 8:00 am - 11:00 am</a></span> 
                    </time>
                  </div>
                </div>

                <div class="col-xs-12  col-sm-6  col-md-3">
                  <div class="widget-single-event">
                    <div class="picture  push-down-15">
                      <a href="#/">
                        <img width="263" height="180" src="<?php echo get_template_directory_uri(); ?>/images/event-2.jpg" class="img-responsive" alt="healthy_breakfast"> 
                        <span class="img-overlay">
                          <span class="btn">Read more</span>
                        </span>
                      </a>
                    </div>
                    <h3 class="widget-single-event-title  no-margin"><a href="#"><span class="light">Italian</span> Night</a></h3>
                    <time class="widget-single-event-date">
                      <span class="entry-date date"><a href="#/" rel="bookmark"><abbr class="dtstart" title="2015-07-08 8:00"></abbr><abbr class="dtend" title="2015-07-08 11:00"></abbr>July 8, 2015 at 8:00 am - 11:00 am</a></span> 
                    </time>
                  </div>
                </div>

                <div class="col-xs-12  col-sm-6  col-md-3">
                  <div class="widget-single-event">
                    <div class="picture  push-down-15">
                      <a href="#/">
                        <img width="263" height="180" src="<?php echo get_template_directory_uri(); ?>/images/event-3.jpg" class="img-responsive" alt="healthy_breakfast"> 
                        <span class="img-overlay">
                          <span class="btn">Read more</span>
                        </span>
                      </a>
                    </div>
                    <h3 class="widget-single-event-title  no-margin"><a href="#"><span class="light">Live</span>  Ambient Music</a></h3>
                    <time class="widget-single-event-date">
                      <span class="entry-date date"><a href="#/" rel="bookmark"><abbr class="dtstart" title="2015-07-08 8:00"></abbr><abbr class="dtend" title="2015-07-08 11:00"></abbr>July 8, 2015 at 8:00 am - 11:00 am</a></span> 
                    </time>
                  </div>
                </div>

                <div class="col-xs-12  col-sm-6  col-md-3">
                  <div class="widget-single-event">
                    <div class="picture  push-down-15">
                      <a href="#/">
                        <img width="263" height="180" src="<?php echo get_template_directory_uri(); ?>/images/event-4.jpg" class="img-responsive" alt="healthy_breakfast"> 
                        <span class="img-overlay">
                          <span class="btn">Read more</span>
                        </span>
                      </a>
                    </div>
                    <h3 class="widget-single-event-title  no-margin"><a href="#"><span class="light">Chinese</span> Day</a></h3>
                    <time class="widget-single-event-date">
                      <span class="entry-date date"><a href="#/" rel="bookmark"><abbr class="dtstart" title="2015-07-08 8:00"></abbr><abbr class="dtend" title="2015-07-08 11:00"></abbr>July 8, 2015 at 8:00 am - 11:00 am</a></span> 
                    </time>
                  </div>
                </div>
              
            </div>
          </div>
        </div>


        <div class="testimonials">
          <div class="title-center">
            <h3 class="widget-title">TESTIMONIALS</h3>  
          </div>
          <div class="carousel  slide" id="widget-4-0-0">
            <ol class="carousel-indicators">
              <li data-target="#widget-4-0-0" data-slide-to="0" class="active"></li>
              <li data-target="#widget-4-0-0" data-slide-to="1" class=""></li>
              <li data-target="#widget-4-0-0" data-slide-to="2" class=""></li>
            </ol>
            <div class="carousel-inner">
              <div class="item active">
                <blockquote class="quote">
                <p>People that made this theme have outstanding and super fast feedback! Keep up the good work guys you made a lovely theme!</p>
                <cite class="author">
                  <div class="author-name">John Doe</div>
                </cite>
                </blockquote>
              </div>
            <div class="item">
              <blockquote class="quote">
                <p>This is a theme that’s 100% perfect for ANY restaurant. All it requires is a change of logo and color scheme …</p>
                <p>I’m happy with all the current functions, especially the slider!</p>
                <cite class="author">
                  <div class="author-name">Luke Skywalker</div>
              </cite>
              </blockquote>
            </div>
              <div class="item">
              <blockquote class="quote">
                <p>Wyaaaaaa. Ruh ruh.&nbsp;Huwaa muaa mumwa.&nbsp;Hnn-rowr yrroonn nng rarrr!&nbsp;Uwana goya uhama.&nbsp;Wah shrf shrf shrf rrrooaarrgghh.&nbsp;Aaawww rooowwr rrrraahhhrr.&nbsp;Rhawk-Arrgh, rrrooaarrgghh!</p>
                <cite class="author">
                  <div class="author-name">Chewbacca</div>
                </cite>
              </blockquote>
              </div>
            </div>
          </div>
        </div>

        <div class="divide-line">
          <div class="icon icons-divider-0"></div>
        </div>

        <div class="bolg-section">  
          <div class="title-center">
            <h3 class="widget-title"><span class="light">Latest</span> Blog Posts</h3>  
          </div>
          <div class="row">
            <article class="col-sm-3">
              <div class="blog-single">
                <div class="picture picture--rounded">
                  <a href="#">
                  <img width="263" height="180" src="<?php echo get_template_directory_uri(); ?>/images/blog-1.jpg" alt="Musicans">  <span class="img-overlay">
                  <span class="btn">Read more</span>
                  </span>
                  </a>
                </div>
                <h3><a href="#"><span class="light">Best</span> Event Ever</a></h3>
                <div class="meta-info">
                  <span class="date">January 15, 2014</span>
                </div>
              </div>
            </article>
            <article class="col-sm-3">
              <div class="blog-single">
                <div class="picture picture--rounded">
                  <a href="#">
                  <img width="263" height="180" src="<?php echo get_template_directory_uri(); ?>/images/blog-2.jpg" alt="Musicans">  <span class="img-overlay">
                  <span class="btn">Read more</span>
                  </span>
                  </a>
                </div>
                <h3><a href="#"><span class="light">Gallery</span> Of Images</a></h3>
                <div class="meta-info">
                  <span class="date">January 15, 2014</span>
                </div>
              </div>
            </article>
            <article class="col-sm-3">
              <div class="blog-single">
                <div class="picture picture--rounded">
                  <a href="#">
                  <img width="263" height="180" src="<?php echo get_template_directory_uri(); ?>/images/blog-3.jpg" alt="Musicans">  <span class="img-overlay">
                  <span class="btn">Read more</span>
                  </span>
                  </a>
                </div>
                <h3><a href="#"><span class="light">Featured</span> Images Post</a></h3>
                <div class="meta-info">
                  <span class="date">January 15, 2014</span>
                </div>
              </div>
            </article>
            <article class="col-sm-3">
              <div class="blog-single">
                <div class="picture picture--rounded">
                  <a href="#">
                  <img width="263" height="180" src="<?php echo get_template_directory_uri(); ?>/images/blog-4.jpg" alt="Musicans">  <span class="img-overlay">
                  <span class="btn">Read more</span>
                  </span>
                  </a>
                </div>
                <h3><a href="#"><span class="light">Embedded</span>  Videos Are Awesome</a></h3>
                <div class="meta-info">
                  <span class="date">January 15, 2014</span>
                </div>
              </div>
            </article>
          </div>
        </div>

      </div>

	    <div class="map-section">
	      <img src="<?php echo get_template_directory_uri(); ?>/images/map.jpg" alt="">
	    </div>


<?php get_footer(); ?>
