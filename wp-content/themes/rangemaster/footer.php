<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	<div class="foot">
		<div class="container">
		<div class="row">
		<div class="col-xs-12  col-md-4  widget--footer"><div class="widget_text">	<div class="textwidget"><div class="center">
		<img src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" alt=""><br><br>
		Example Street 546<br>
		Creative Town, ZIP 12345<br>
		Slovenia, EU<br><br>
		<a href="#">SEE MAP &gt;</a>
		</div></div>
		</div></div><div class="col-xs-12  col-md-4  widget--footer"><div class="opening-time  chalkboard-bg"><span class="corner-top-left"></span>
		<span class="corner-top-right"></span>
		<span class="corner-bottom-left"></span>
		<span class="corner-bottom-right"></span>
		<div class="time-table">
		<h2 class="widget-title"><span class="light">Opening</span> Times</h2><div class="inner-bg">
		<dl class="week-day closed">
		<dt>Monday</dt>
		<dd>CLOSED</dd>
		</dl>
		<dl class="week-day light-bg closed today">
		<dt>Tuesday</dt>
		<dd>CLOSED</dd>
		</dl>
		<dl class="week-day">
		<dt>Wednesday</dt>
		<dd>11 am - 10 pm</dd>
		</dl>
		<dl class="week-day light-bg">
		<dt>Thursday</dt>
		<dd>11 am - 10 pm</dd>
		</dl>
		<dl class="week-day">
		<dt>Friday</dt>
		<dd>11 am - 2 am</dd>
		</dl>
		<dl class="week-day light-bg">
		<dt>Saturday</dt>
		<dd>1 pm - 4 am</dd>
		</dl>
		<dl class="week-day">
		<dt>Sunday</dt>
		<dd>11 am - 1 am</dd>
		</dl>
		</div>
		</div>
		</div></div><div class="col-xs-12  col-md-4  widget--footer"><div class="widget_text"><h2 class="widget-title"><span class="light">Get</span> in Touch</h2>	<div class="textwidget"><div class="center">
		Phone: <a href="#">+386 01 2345-6789</a><br>
		Fax: <a href="#">+386 01 2345-6789</a><br>
		mail: <a href="mailto:info@example.com">info@example.com<script cf-hash="f9e31" type="text/javascript">
		/* <![CDATA[ */!function(){try{var t="currentScript"in document?document.currentScript:function(){for(var t=document.getElementsByTagName("script"),e=t.length;e--;)if(t[e].getAttribute("cf-hash"))return t[e]}();if(t&&t.previousSibling){var e,r,n,i,c=t.previousSibling,a=c.getAttribute("data-cfemail");if(a){for(e="",r=parseInt(a.substr(0,2),16),n=2;a.length-n;n+=2)i=parseInt(a.substr(n,2),16)^r,e+=String.fromCharCode(i);e=document.createTextNode(e),c.parentNode.replaceChild(e,c)}}}catch(u){}}();/* ]]> */</script></a><br>
		web: <a href="http://www.proteusthemes.com/" target="_blank">www.proteusthemes.com</a>
		</div>
		</div>
		</div></div>
		</div><!-- /row -->
		</div><!-- /container -->
	</div>


	<footer class="footer">
		<a href="#" id="to-the-top" class="btn  btn-theme">
		<span class="fa  fa-angle-up  fa-2x"></span>
		</a>
		<div class="container">
		<div class="footer__left">
		© Copyright 2014	</div>
		<div class="footer__right">
		Restaurant Theme by <a href="http://www.proteusthemes.com">ProteusThemes</a>	</div>
		</div>
	</footer>
</div><!-- .wrapper -->
</div><!--main-->
<?php wp_footer(); ?>

</body>
</html>
