<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="container">
                <div class="row">
                    <div class="title-area">
                        <div class="container">
                            <h1>Contact Us</h1>
                        </div>
                    </div>
                    <div class="breadcrumbs-container">
                        <div class="container breadcrumb">
                            <!-- Breadcrumb NavXT 5.2.0 -->
                            <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to Dining Restaurant." href="/restaurant" class="home">Dining Restaurant</a></span><span class="separator">&gt;</span><span typeof="v:Breadcrumb"><span property="v:title">Contact Us</span></span>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12  col-md-12" role="main">
                                    <article class="post-1221 page type-page status-publish">
                                        <div class="hentry__content">
                                            <p>You can contact us many different ways. The easiest one is to fill out the form below and press <strong>Send</strong>.</p>
                                            <div class="wpcf7" id="wpcf7-f6-p1221-o1" lang="en-US" dir="ltr">
                                                <div class="screen-reader-response"></div>
                                                <form name="" action="/restaurant/contact-us/#wpcf7-f6-p1221-o1" method="post" class="wpcf7-form" novalidate="novalidate">

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                Your Name (required):
                                                                <br>
                                                                <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false"></span>
                                                            </div>
                                                            <div class="col-md-4">
                                                                Your Email (required):
                                                                <br>
                                                                <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false"></span>
                                                            </div>
                                                            <div class="col-md-4">
                                                                Your Phone:
                                                                <br>
                                                                <span class="wpcf7-form-control-wrap your-phone"><input type="tel" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel form-control" aria-invalid="false"></span>
                                                            </div>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                Your message:
                                                                <br>
                                                                <span class="wpcf7-form-control-wrap x5"><textarea name="x5" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea form-control" aria-invalid="false"></textarea></span>
                                                            </div>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit btn btn-theme"><img class="ajax-loader" src="images/ajax-loader.gif" alt="Sending ..." style="visibility: hidden;">
                                                            </div>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                </form>
                                            </div>
                                            <hr>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa.</p>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="map-section">
                            <img src="images/map.jpg" alt="">
                        </div> -->

                    </div>
                </div>
            </div>
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		// End the loop.
		endwhile;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
