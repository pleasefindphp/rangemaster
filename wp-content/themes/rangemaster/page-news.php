<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div class="container">
    <div class="row">
        <div class="title-area">
            <div class="container">
                <h1>What's happening around?</h1>
            </div>
        </div>
        <div class="breadcrumbs-container">
            <div class="container breadcrumb">
                <!-- Breadcrumb NavXT 5.2.0 -->
                <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to Dining Restaurant." href="#/" class="home">Dining Restaurant</a></span><span class="separator">&gt;</span><span typeof="v:Breadcrumb"><span property="v:title">News</span></span>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12  col-md-9 " role="main">
                        <div class="row">
                        	<?php
			            		$loop = new WP_Query( array( 'post_type' => 'news') );
			                    while ( $loop->have_posts() ) : $loop->the_post();  
			            	?>
                            <div class="col-xs-12">
                            <article class="post-188 post type-post status-publish format-standard has-post-thumbnail sticky category-rangemaster tag-sticky tag-template post-inner">
  								<!-- youtube video -->	                          
                            	<?php if(get_post_custom_values('youtubeEmpbedVideoLink')[0]) {  ?>
                                <div class="embed-responsive  embed-responsive-16by9">
                                    <iframe width="848" height="477" src="<?php echo get_post_custom_values('youtubeEmpbedVideoLink')[0]; ?>" frameborder="0" allowfullscreen="" s2431518236969637720="true" replaced="true"></iframe>
                                </div>
                                <?php } ?>
                                <!-- youtube video -->
                                <!-- featured image -->
                                <?php 
                                    if (has_post_thumbnail()) {
                                        $attrs = array(
											"width" => 848,
											"height" => 265,
											"class" => "img-responsive wp-post-image",
											"alt" => the_title(),
										);
                                        the_post_thumbnail( 'thumbnail', $attrs );    
                                    }
                                ?>
                                <!-- featured image -->
                                <h1><a href="<?php the_permalink(); ?>"><span class="light"><?php the_title(); ?></a></h1>

                                <div class="hentry__content">
                                    <p> Primož Cigler Primož Cigler v Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler v Primož Cigler Primož Cigler v Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler v </p>
                                    <p> Primož Cigler Primož Cigler v Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler v Primož Cigler Primož Cigler v Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler v </p>
                                    <p>?Primož Cigler Primož Cigler v Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler Primož Cigler v</p>
                                </div>
                                <div class="clearfix"></div>
                            </article>
                        </div>
                        <!-- /blogpost -->
                        <div class="col-xs-12">
                            <div class="divide-line">
                                <div class="icon icons-divider-0"></div>
                            </div>
                        </div>
                    	<?php endwhile; ?>
                        </div>
                    </div>
                    <!-- /blog -->
                    <div class="col-xs-12  col-md-3 ">
                        <div class="sidebar  push-down-30">
                        	<?php dynamic_sidebar( 'sidebar-1' ) ?>
                            <!-- <div id="search-2" class="sidebar-item widget_search">
                                <h2 class="widget-title"><span class="light">Search</span> the news</h2>
                                <form role="search" method="get" id="searchform" action="/rangemaster/" class="form">
                                    <div class="input-append">
                                        <input name="s" class="search-width" id="appendedInputButton" type="search" value="">
                                        <button class="btn btn-theme" type="submit">Go</button>
                                    </div>
                                </form>
                            </div>
                            <div id="recent-posts-2" class="sidebar-item widget_recent_entries">
                                
                                <h2 class="widget-title"><span class="light">Recent</span> Posts</h2>
                                <ul>
                                    <li>
                                        <a href="/restaurant/2014/01/15/best-event-ever/">Best Event Ever</a>
                                    </li>
                                    <li>
                                        <a href="">Gallery Of Images</a>
                                    </li>
                                    <li>
                                        <a href="">Featured Image Post</a>
                                    </li>
                                    <li>
                                        <a href="/restaurant/2014/01/12/post-format-test-video/">Embedded Videos Are Awesome</a>
                                    </li>
                                    <li>
                                        <a href="/restaurant/2014/01/11/template-more-tag/">Template: More tag</a>
                                    </li>
                                </ul>
                            </div>
                            <div id="recent-comments-2" class="sidebar-item widget_recent_comments">
                                <h2 class="widget-title"><span class="light">Recent</span> Comments</h2>
                                <ul id="recentcomments">
                                    <li class="recentcomments"><span class="comment-author-link"><a href="http://www.fresghervelly.com" rel="external nofollow" class="url">ramsaran</a></span> on <a href="/restaurant/2014/01/11/template-more-tag/#comment-86">Template: More tag</a>
                                    </li>
                                    <li class="recentcomments"><span class="comment-author-link"><a href="http://photomatt.net/" rel="external nofollow" class="url">Matt</a></span> on <a href="/restaurant/2013/12/10/comment-test/#comment-68">Lots of Comments</a>
                                    </li>
                                    <li class="recentcomments"><span class="comment-author-link"><a href="http://foolswisdom.com" rel="external nofollow" class="url">Lloyd Budd</a></span> on <a href="/restaurant/2013/12/10/comment-test/#comment-69">Lots of Comments</a>
                                    </li>
                                    <li class="recentcomments"><span class="comment-author-link"><a href="http://ntutest.wordpress.com/2007/11/19/pinging-like-a-microwave/" rel="external nofollow" class="url">pinging like a microwave « no kubrick allowed</a></span> on <a href="/restaurant/2013/12/10/comment-test/#comment-75">Lots of Comments</a>
                                    </li>
                                    <li class="recentcomments"><span class="comment-author-link"><a href="http://flightpath.wordpress.com/" rel="external nofollow" class="url">Alex Shiels</a></span> on <a href="/restaurant/2013/12/10/comment-test/#comment-66">Lots of Comments</a>
                                    </li>
                                </ul>
                            </div>
                            <div id="archives-2" class="sidebar-item widget_archive">
                                <h2 class="widget-title"><span class="light">Browse</span> the history</h2>
                                <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                                    <option value="">Select Month</option>
                                    <option value="/restaurant/2014/01/"> January 2014 &nbsp;(8)</option>
                                    <option value="/restaurant/2013/12/"> December 2013 &nbsp;(3)</option>
                                </select>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



<?php get_footer(); ?>
