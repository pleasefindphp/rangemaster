<div id="search-2" class="sidebar-item widget_search">
    <h2 class="widget-title"><span class="light">Search</span> the news</h2>
    <form role="search" method="get" id="searchform" action="/rangemaster/" class="form">
        <div class="input-append">
            <input name="s" class="search-width" id="appendedInputButton" type="search" value="">
            <button class="btn btn-theme" type="submit">Go</button>
        </div>
    </form>
</div>