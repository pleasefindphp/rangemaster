<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<!-- awards page design -->

		<div class="container">
                <div class="row">
                    <div class="title-area">
                        <div class="container">
                            <h1>Whare we stand?</h1>
                        </div>
                    </div>
                    <div class="breadcrumbs-container">
                        <div class="container breadcrumb">
                            <!-- Breadcrumb NavXT 5.2.0 -->
                            <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Go to Dining Restaurant." href="#/" class="home">Dining Restaurant</a></span><span class="separator">&gt;</span><span typeof="v:Breadcrumb"><span property="v:title">Awards</span></span>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="summary">
                                    <?php while ( have_posts() ) : the_post();	?>
                                    <div class="font-text-large">
									<?php the_content(); ?>
									</div>
									<?php endwhile; ?>		
                                    
                                </div>
                                <?php
                                	$loop = new WP_Query( array( 'post_type' => 'awards') );
                                	while ( $loop->have_posts() ) : $loop->the_post();  
                                ?>
                                <!-- looping -->
                                <div class="col-xs-6  col-md-6 " role="main">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <article class="post-188 post type-post status-publish format-standard has-post-thumbnail sticky category-rangemaster tag-sticky tag-template post-inner">
                                                <a href="">
                                                    <!-- <img width="848" height="265" src="images/blogs_image1.webp" class="img-responsive wp-post-image" alt="Bar"> </a> -->
                                                    <?php
                                                    $attrs = array(
														"width" => "848", 
														"height" => "265",  
														"class" => "img-responsive wp-post-image", 
														"alt" => "Bar"
													);
                                                    if (has_post_thumbnail()) {
                                                        the_post_thumbnail( 'thumbnail', $attrs );    
                                                    }
                                                    ?>
                                                <h1><a href="<?php the_permalink(); ?>"><span class="light"><?php the_title(); ?></a></h1>

                                                <div class="hentry__content">
                                                  	<?php the_content(); ?>  
                                                </div>
                                                <div class="clearfix"></div>
                                            </article>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="divide-line">
                                                <div class="icon icons-divider-0"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endwhile;  ?>
                                <!-- looping -->
                            </div>
                        </div>

                    </div>
                </div>
            </div>

		<!-- awards page design -->

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
