<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rangemaster');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ek$;-x?-;0%iJ@l|Ot9Jn(FU[k2jz,vxc^lU%+d0O13m$6cBQ%S$A!c69:JkKa?F');
define('SECURE_AUTH_KEY',  ')P6da~ 1bct)WUBMrcUBMj0kcdKNmOwwOt|hXVM~0$=hW<uIj-sp~HByM-`(TD,k');
define('LOGGED_IN_KEY',    'L3@|Bkj%HKJWWabAl-W=z+&I0N|C++l6%n#Lx+)In-j{j~+^C_;&PRFR8qr|o~ol');
define('NONCE_KEY',        'SF!qw&Iz%H*4])^_QuNdL$0@Gz6YPoh}~py3QR+9>LlRLF^#POvZ1++Z[2pzIZEa');
define('AUTH_SALT',        'N ??D4/XBS+k}j%eZ^gMMCboF[Md_#91?(cJPE=1n2~rQ#sCP:^kAd@X lw:7IBz');
define('SECURE_AUTH_SALT', 's<#bRa #KM,LXYqf_OYec3%n d6NBT8!J Lh59&Yyx68G3?nJZxA{v?0qIt&K2M_');
define('LOGGED_IN_SALT',   'g[sYAtW4q+I6H(X!atVwQpUOkm-`IAYBK2O~Yoeg/U=aOSQMx&]0zD-NbDk|=Om2');
define('NONCE_SALT',       '8}x}U+S?NZN8L_lsKYlvu~jjPXSYmce%?mIu1(=KxB+e69s-dm|N-oB`B$?a[4,<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp7664786784_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
